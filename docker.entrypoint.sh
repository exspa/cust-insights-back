#!/usr/bin/env sh

main () {
  if [ "$TEST_RUN" = "True" ]; then
    export SECRET_KEY=$(tr -dc 'a-z0-9' < /dev/urandom | head -c50);
    gunicorn "$@" user_research.wsgi:application --capture-output --log-level 'debug'
  else
    export ALLOWED_HOSTS="localhost,127.0.0.1,$ALLOWED_HOSTS";
    check_code=1000
    while [ $check_code -ge 1 ]
    do
      python3 conn_info.py
      check_code=$?
      sleep 3
    done
    python manage.py makemigrations &&
    python manage.py migrate &&
    gunicorn "$@" user_research.wsgi:application --capture-output
  fi
}
main "$@"
