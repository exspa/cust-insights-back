<a href="https://custinsights.ru/sign-up/researcher">
    <img src="https://s3.custinsights.ru/front-static/Logo_white.png" width="188" height="50">
</a>

<hr>

[![](https://img.shields.io/badge/Django-v4.2.4-green)](https://www.djangoproject.com/)
[![](https://img.shields.io/badge/DRF-v3.14.0-A30000)](https://www.django-rest-framework.org/)
[![](https://img.shields.io/badge/PostgreSQL-v14.9-blue)](https://www.postgresql.org/)
[![](https://gitlab.com/roman-devops33/user-research-back/badges/dev/pipeline.svg)](https://gitlab.com/roman-devops33/user-research-back/-/pipelines)
[![](https://kuma.jetstreamsam.tk/api/badge/14/status)](https://api.custinsights.ru/ht/)


## Описание

**CustInsights** – платформа для автоматизации процессов поиска и исследования ЦА в сферах B2B и B2C.
- Первая в РФ единая платформа для проведения опросов и глубинных интервью
- Собственная база качественных респондентов для бизнеса в сферах B2B и B2C
- Автоматизация процессов поиска респондентов, обработки, аналитики и выгрузки данных

## Функционал

#### Стек технологий:
- **Фронтенд**: JavaScript, React,
- **Бэкенд** Python, Django, Django Rest Framework
- **База данных**: PostgreSQL

#### Имплементация и используемые инструменты:
- [Документация API](https://api.custinsights.ru/swagger/) на базе Swagger, ReDoc - `drf-yasg`
- Аутентификация на базе JSON Web Token -`drf-simple-jwt`
- Управление пользователями API - `djoser`
- Создание и обновление объектов вложенных сериализаторов - `drf-writable-nested`
- Интерфейс респондента:
  - Анкета респондента ([базовая](https://s3.custinsights.ru/front-static/respondent_basic_form.png), [расширенная](https://gitlab.com/roman-devops33/user-research-back/-/wikis/assets/respondent_extend_form.png), [для экспертов](https://s3.custinsights.ru/front-static/respondent_expert_form.png))
- Интерфейс исследователя:
  - [Создание и публикация проекта](https://s3.custinsights.ru/front-static/research_project.png)
  - [Конструктор опросов](https://s3.custinsights.ru/front-static/poll_designer.png)

#### В процессе реализации:
- Асинхронное выполнение задач по email рассылке и другое - `Celery` + `Redis`
- Оптимизация бэкенд приложения - `Django Debug Toolbar`
- Дашборды с виджетами
- Аналитика опросов
- Функционал видеоинтервью и аудиоинтервью
- Voice-to-text
- Архив записей интервью
- Календарь с выбором слотов интервью
- Чат
- Баланс и вывод средств
- Мэтчинг-алгоритм проектов по параметрам и интересам ЦА


[//]: # (## Установка и запуск)

[//]: # (- ...)
