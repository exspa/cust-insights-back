# Versions
ARG PYTHON=python:3.9-alpine
ARG BUILDER
## Stage one ##
FROM $BUILDER AS build
WORKDIR /usr/app

## Stage two ##
FROM $PYTHON
ARG BUILD_VERSION

# Установка зависимостей C, создание пользователя
RUN apk add --no-cache \
            libusb-dev \
            py3-magic
RUN adduser -D cobra
RUN mkdir /usr/app /data
RUN chown cobra:cobra /usr/app /data
WORKDIR /usr/app

# Подготовка кода и окружения
COPY --chown=cobra:cobra --from=build /usr/app/venv ./venv
COPY --chown=cobra:cobra /user_research .
COPY --chown=cobra:cobra --chmod=700 docker.entrypoint.sh entrypoint.sh

# Settings
USER cobra
ENV PATH="/usr/app/venv/bin:$PATH"
EXPOSE 8000

# Collect data
RUN mkdir -p /data/migrations/django
RUN for dir in /usr/app/*/migrations; do \
        mv $dir /data/migrations/`echo $dir | cut -d "/" -f4` && \
        ln -s /data/migrations/`echo $dir | cut -d "/" -f4` $dir; \
      done

# Django migrations
RUN mkdir /data/migrations/django/auth
RUN mv -f /usr/app/venv/lib/python3*/site-packages/django/contrib/auth/migrations/* \
          /data/migrations/django/auth/
RUN rm -r /usr/app/venv/lib/python3.[0-9]*/site-packages/django/contrib/auth/migrations
RUN ln -s /data/migrations/django/auth/ \
          `echo /usr/app/venv/lib/python3.[0-9]*/site-packages/django/contrib/auth`/migrations

# Переменные среды
ENV BUILD_VERSION=${BUILD_VERSION:-noversion}

# Сервисные переменные
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# Metadata
LABEL app.version="${BUILD_VERSION}" \
      org.telegram.autor-link="https://t.me/salixtein"

# Запуск
ENTRYPOINT ["/usr/app/entrypoint.sh"]
CMD ["--bind", "0.0.0.0:8000", "--workers=1"]
HEALTHCHECK --interval=7s --timeout=3s --start-period=10s \
        CMD wget --no-verbose --tries=1 --spider http://localhost:8000/ht/

