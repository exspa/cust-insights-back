#!/bin/bash

echo "Loading data from fixtures..."

python manage.py loaddata matching/fixtures/researchtype_data.json
python manage.py loaddata matching/fixtures/gender_data.json
python manage.py loaddata matching/fixtures/education_data.json
python manage.py loaddata matching/fixtures/familystatus_data.json
python manage.py loaddata matching/fixtures/children_data.json
python manage.py loaddata matching/fixtures/income_data.json
python manage.py loaddata matching/fixtures/browser_data.json
python manage.py loaddata matching/fixtures/operatingsystem_data.json
python manage.py loaddata matching/fixtures/interestarea_data.json
python manage.py loaddata matching/fixtures/companytype_data.json
python manage.py loaddata matching/fixtures/numberofemployees_data.json
python manage.py loaddata matching/fixtures/employeeposition_data.json
python manage.py loaddata matching/fixtures/businessarea_data.json
python manage.py loaddata matching/fixtures/country_data.json
python manage.py loaddata matching/fixtures/region_data.json
python manage.py loaddata matching/fixtures/city_data.json

echo "Data loading completed!"
