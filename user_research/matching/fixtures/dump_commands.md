```bash
python manage.py dumpdata matching.researchtype > matching/fixtures/researchtype_data.json
```
```bash
python manage.py dumpdata matching.gender > matching/fixtures/gender_data.json
```
```bash
python manage.py dumpdata matching.education > matching/fixtures/education_data.json
```
```bash
python manage.py dumpdata matching.familystatus > matching/fixtures/familystatus_data.json
```
```bash
python manage.py dumpdata matching.children > matching/fixtures/children_data.json
```
```bash
python manage.py dumpdata matching.income > matching/fixtures/income_data.json
```
```bash
python manage.py dumpdata matching.browser > matching/fixtures/browser_data.json
```
```bash
python manage.py dumpdata matching.operatingsystem > matching/fixtures/operatingsystem_data.json
```
```bash
python manage.py dumpdata matching.interestarea > matching/fixtures/interestarea_data.json
```
```bash
python manage.py dumpdata matching.companytype > matching/fixtures/companytype_data.json
```
```bash
python manage.py dumpdata matching.numberofemployees > matching/fixtures/numberofemployees_data.json
```
```bash
python manage.py dumpdata matching.employeeposition > matching/fixtures/employeeposition_data.json
```
```bash
python manage.py dumpdata matching.businessarea > matching/fixtures/businessarea_data.json
```
```bash
python manage.py dumpdata matching.country > matching/fixtures/country_data.json
```
```bash
python manage.py dumpdata matching.region > matching/fixtures/region_data.json
```
```bash
python manage.py dumpdata matching.city > matching/fixtures/city_data.json
```
