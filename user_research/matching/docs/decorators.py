from drf_yasg.utils import swagger_auto_schema
from rest_framework import status

from . import serializers


def swagger_auto_schema_for_matching_retrieve(view_func):
    return swagger_auto_schema(
        responses={
            status.HTTP_200_OK: serializers.MatchingPropertySerializer(),
            status.HTTP_404_NOT_FOUND: 'Not found',
        },
        operation_summary='Get property object by id',
    )(view_func)


def swagger_auto_schema_for_matching_list(view_func):
    return swagger_auto_schema(
        responses={
            status.HTTP_200_OK: serializers.MatchingPropertySerializer(many=True),
            status.HTTP_404_NOT_FOUND: 'Not found',
        },
        operation_summary='Get objects list  by property name',
    )(view_func)


def swagger_auto_schema_for_matching_all_properties(view_func):
    return swagger_auto_schema(
        methods=['get'],
        responses={
            status.HTTP_200_OK: serializers.AllMatchingPropertiesSerializer(),
        },
        operation_summary='Get all properties',
    )(view_func)
