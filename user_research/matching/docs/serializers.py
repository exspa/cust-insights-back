from rest_framework import serializers

from matching import serializers as matching_serializers


class MatchingPropertySerializer(matching_serializers.GenderSerializer):
    pass


class AllMatchingPropertiesSerializer(serializers.Serializer):
    children = matching_serializers.ChildrenSerializer()
    browser = matching_serializers.BrowserSerializer()
    gender = matching_serializers.GenderSerializer()
    other_properties = MatchingPropertySerializer()
