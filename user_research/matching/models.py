from django.db import models


class AbstractMatchingModel(models.Model):
    """
    Abstract base model with common fields and methods for derived models.

    Provides common structure and behavior for models that require a title
    and a sort order. The verbose name is used to provide a more descriptive
    string representation for the instances of subclasses.
    """

    title = models.CharField('value of the property', max_length=200)

    sort_order = models.PositiveSmallIntegerField(
        'sorting order', unique=True
    )

    class Meta:
        abstract = True
        verbose_name = 'name not defined'
        ordering = ['sort_order']

    def __str__(self):
        return f'{self.title}'


class ResearchType(AbstractMatchingModel):
    class Meta(AbstractMatchingModel.Meta):
        verbose_name = 'type of research'
        verbose_name_plural = 'types of research'


class Gender(AbstractMatchingModel):
    class Meta(AbstractMatchingModel.Meta):
        verbose_name = 'gender'
        verbose_name_plural = 'gender'


class Education(AbstractMatchingModel):
    class Meta(AbstractMatchingModel.Meta):
        verbose_name = 'level of education'
        verbose_name_plural = 'levels of education'


class FamilyStatus(AbstractMatchingModel):
    class Meta(AbstractMatchingModel.Meta):
        verbose_name = 'family status'
        verbose_name_plural = 'family status'


class Children(AbstractMatchingModel):
    class Meta(AbstractMatchingModel.Meta):
        verbose_name = 'amount of children'
        verbose_name_plural = 'amount of children'


class Income(AbstractMatchingModel):
    class Meta(AbstractMatchingModel.Meta):
        verbose_name = 'average monthly income'
        verbose_name_plural = 'average monthly income'


class Browser(AbstractMatchingModel):
    class Meta(AbstractMatchingModel.Meta):
        verbose_name = 'browser'
        verbose_name_plural = 'browsers'


class OperatingSystem(AbstractMatchingModel):
    class Meta(AbstractMatchingModel.Meta):
        verbose_name = 'operating system'
        verbose_name_plural = 'operating systems'


class InterestArea(AbstractMatchingModel):
    class Meta(AbstractMatchingModel.Meta):
        verbose_name = 'area of interest'
        verbose_name_plural = 'areas of interest'


class CompanyType(AbstractMatchingModel):
    class Meta(AbstractMatchingModel.Meta):
        verbose_name = 'ownership type'
        verbose_name_plural = 'ownership types'


class NumberOfEmployees(AbstractMatchingModel):
    class Meta(AbstractMatchingModel.Meta):
        verbose_name = 'number of employees'
        verbose_name_plural = 'number of employees'


class EmployeePosition(AbstractMatchingModel):
    class Meta(AbstractMatchingModel.Meta):
        verbose_name = 'employee\'s position'
        verbose_name_plural = 'positions of an employee'


class BusinessArea(AbstractMatchingModel):
    class Meta(AbstractMatchingModel.Meta):
        verbose_name = 'business area'
        verbose_name_plural = 'business areas'


class Country(models.Model):
    title = models.CharField('country', max_length=100)

    class Meta:
        verbose_name = 'country'
        verbose_name_plural = 'countries'
        ordering = ['title']

    def __str__(self):
        return f'{self.title}'


class Region(models.Model):
    title = models.CharField('region', max_length=100)

    country = models.ForeignKey(
        Country,
        on_delete=models.PROTECT,
        related_name='regions',
        verbose_name='country',
    )

    class Meta:
        verbose_name = 'region'
        verbose_name_plural = 'regions'
        ordering = ['title']

    def __str__(self):
        return f'{self.title}, {self.country}'


class City(models.Model):
    title = models.CharField('city', max_length=100)

    region = models.ForeignKey(
        Region, on_delete=models.PROTECT, related_name='cities', verbose_name='regions'
    )

    class Meta:
        verbose_name = 'city'
        verbose_name_plural = 'cities'
        ordering = ['title']

    def __str__(self):
        return f'{self.title}, {self.region}'


class DeviceType(models.TextChoices):
    COMPUTER = 'computer', 'компьютер'
    TABLET = 'tablet', 'планшет'
    SMARTPHONE = 'smartphone', 'смартфон'


class Device(models.Model):
    """
    Represents a respondent's device, including type, operating system,
    and browser.
    """

    device_type = models.CharField(
        'type of device', max_length=20, choices=DeviceType.choices
    )

    operating_sys = models.ManyToManyField(
        OperatingSystem,
        related_name='devices',
        verbose_name='operating system',
    )
    browser = models.ManyToManyField(
        Browser,
        related_name='devices',
        verbose_name='browser',
    )

    class Meta:
        verbose_name = 'device'
        verbose_name_plural = 'devices'
        ordering = ['device_type']

    def __str__(self):
        browsers = ', '.join(str(browser) for browser in self.browser.all())
        os = ', '.join(str(os) for os in self.operating_sys.all())
        return f'{self.id} - {self.device_type.title()}: [{browsers}] / [{os}]'
