from django.urls import path

from . import views


urlpatterns = [
    path(
        'matching/all_properties/',
        views.MatchingViewSet.as_view({'get': 'all_properties'}),
        name='all_properties'
    ),
    path(
        'matching/<str:property_name>/',
        views.MatchingViewSet.as_view({'get': 'list'}),
        name='matching_list'
    ),
    path(
        'matching/<str:property_name>/<int:pk>/',
        views.MatchingViewSet.as_view({'get': 'retrieve'}),
        name='matching_detail'
    ),
]
