from rest_framework import serializers

from . import models as matching_models


class MatchingSerializer(serializers.ModelSerializer):
    """
    A base serializer for matching models with common fields and validations.
    """

    class Meta:
        model = None
        fields = ('id', 'title')
        extra_kwargs = {
            'id': {'read_only': False, 'required': True},
            'title': {'read_only': True},
        }

    @property
    def model_class(self):
        return self.Meta.model


def create_matching_serializer(model):
    """Dynamically create a serializer class for a given model."""

    class Meta(MatchingSerializer.Meta):
        pass

    Meta.model = model
    return type(f'{model.__name__}Serializer', (MatchingSerializer,), {'Meta': Meta})


ResearchTypeSerializer = create_matching_serializer(matching_models.ResearchType)
GenderSerializer = create_matching_serializer(matching_models.Gender)
EducationSerializer = create_matching_serializer(matching_models.Education)
FamilyStatusSerializer = create_matching_serializer(matching_models.FamilyStatus)
ChildrenSerializer = create_matching_serializer(matching_models.Children)
IncomeSerializer = create_matching_serializer(matching_models.Income)
InterestAreaSerializer = create_matching_serializer(matching_models.InterestArea)
CompanyTypeSerializer = create_matching_serializer(matching_models.CompanyType)
BusinessAreaSerializer = create_matching_serializer(matching_models.BusinessArea)
NumberOfEmployeesSerializer = create_matching_serializer(
    matching_models.NumberOfEmployees
)
EmployeePositionSerializer = create_matching_serializer(
    matching_models.EmployeePosition
)


class CountrySerializer(MatchingSerializer):
    class Meta:
        model = matching_models.Country
        fields = ('id', 'title')


class RegionSerializer(MatchingSerializer):
    country = CountrySerializer()

    class Meta:
        model = matching_models.Region
        fields = ('id', 'title', 'country')


class CitySerializer(MatchingSerializer):
    region = RegionSerializer()

    class Meta:
        model = matching_models.City
        fields = ('id', 'title', 'region')


class BrowserSerializer(MatchingSerializer):
    class Meta:
        model = matching_models.Browser
        fields = ('id', 'title')
        extra_kwargs = {
            'id': {'read_only': False, 'required': True},
            'title': {'read_only': True},
        }


class OperatingSystemSerializer(MatchingSerializer):
    class Meta:
        model = matching_models.OperatingSystem
        fields = ('id', 'title')
        extra_kwargs = {
            'id': {'read_only': False, 'required': True},
            'title': {'read_only': True},
        }


class DeviceSerializer(serializers.ModelSerializer):
    browser = BrowserSerializer(many=True)
    operating_sys = OperatingSystemSerializer(many=True)

    class Meta:
        model = matching_models.Device
        fields = ('id', 'device_type', 'browser', 'operating_sys')
