from django.contrib import admin

from . import models


class MatchingModelAdmin(admin.ModelAdmin):
    """
    Base admin configuration for models that share a common display pattern.

    This is an abstract base configuration meant to be subclassed,
    and not used directly. It's optimized for models that have similar
    characteristics such as having a sort order, an ID, and a title
    that needs to be displayed in the Django admin interface.
    """

    list_display = (
        'id',
        'sort_order',
        'title',
    )
    list_display_links = (
        'id',
        'title',
    )


models_to_register = [
    models.ResearchType,
    models.Gender,
    models.Education,
    models.FamilyStatus,
    models.Children,
    models.Income,
    models.Browser,
    models.OperatingSystem,
    models.InterestArea,
    models.CompanyType,
    models.NumberOfEmployees,
    models.EmployeePosition,
    models.BusinessArea,
]

for model in models_to_register:
    admin.site.register(model, MatchingModelAdmin)


@admin.register(models.Device)
class DeviceAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'device_type',
    )
    list_display_links = (
        'id',
        'device_type',
    )
    search_fields = ('device_type',)


@admin.register(models.Country)
class CountryAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'title',
    )
    list_display_links = (
        'id',
        'title',
    )
    search_fields = ('title',)


@admin.register(models.Region)
class RegionAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'title',
        'country',
    )
    list_display_links = (
        'id',
        'title',
    )
    search_fields = (
        'title',
        'country',
    )


@admin.register(models.City)
class CityAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'title',
        'region',
    )
    list_display_links = (
        'id',
        'title',
    )
    search_fields = (
        'title',
        'region',
    )
