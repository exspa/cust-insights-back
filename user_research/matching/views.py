from django.db.models import QuerySet
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.exceptions import NotFound
from rest_framework.response import Response

from . import models
from . import serializers
from .docs import decorators


class MatchingViewSet(viewsets.ReadOnlyModelViewSet):
    MAPPING_PROPERTIES = {
        'children': (serializers.ChildrenSerializer, models.Children.objects.all()),
        'browser': (serializers.BrowserSerializer, models.Browser.objects.all()),
        'research_type': (
            serializers.ResearchTypeSerializer,
            models.ResearchType.objects.all(),
        ),
        'gender': (
            serializers.GenderSerializer,
            models.Gender.objects.all(),
        ),
        'education': (
            serializers.EducationSerializer,
            models.Education.objects.all(),
        ),
        'family_status': (
            serializers.FamilyStatusSerializer,
            models.FamilyStatus.objects.all(),
        ),
        'income': (
            serializers.IncomeSerializer,
            models.Income.objects.all(),
        ),
        'interest_area': (
            serializers.InterestAreaSerializer,
            models.InterestArea.objects.all(),
        ),
        'company_type': (
            serializers.CompanyTypeSerializer,
            models.CompanyType.objects.all(),
        ),
        'number_of_employees': (
            serializers.NumberOfEmployeesSerializer,
            models.NumberOfEmployees.objects.all(),
        ),
        'employee_position': (
            serializers.EmployeePositionSerializer,
            models.EmployeePosition.objects.all(),
        ),
        'business_area': (
            serializers.BusinessAreaSerializer,
            models.BusinessArea.objects.all(),
        ),
        'operating_system': (
            serializers.OperatingSystemSerializer,
            models.OperatingSystem.objects.all(),
        ),
        'country': (
            serializers.CountrySerializer,
            models.Country.objects.all(),
        ),
        'region': (
            serializers.RegionSerializer,
            models.Region.objects.all(),
        ),
        'city': (
            serializers.CitySerializer,
            models.City.objects.all(),
        ),
    }

    @property
    def property_name(self):
        return self.kwargs.get('property_name', None)

    def initial(self, request, *args, **kwargs):
        super().initial(request, *args, **kwargs)
        if (
            self.property_name not in self.MAPPING_PROPERTIES
            and self.action != 'all_properties'
        ):
            raise NotFound()

    def get_serializer_class(self):
        return self.MAPPING_PROPERTIES.get(self.property_name)[0]

    def get_queryset(self):
        if getattr(self, 'swagger_fake_view', False):
            return QuerySet()
        return self.MAPPING_PROPERTIES.get(self.property_name)[1]

    @decorators.swagger_auto_schema_for_matching_retrieve
    def retrieve(self, request, *args, **kwargs):
        return super().retrieve(request, *args, **kwargs)

    @decorators.swagger_auto_schema_for_matching_list
    def list(self, request, *args, **kwargs):
        return super().list(request, *args, **kwargs)

    @decorators.swagger_auto_schema_for_matching_all_properties
    @action(methods=['get'], detail=False, url_path='all_properties')
    def all_properties(self, request):
        data = {}
        for property_name, (
            serializer_class,
            queryset,
        ) in self.MAPPING_PROPERTIES.items():
            if property_name not in ('country', 'region', 'city'):
                data[property_name] = serializer_class(queryset, many=True).data
        return Response(data)
