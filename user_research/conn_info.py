import os
import psycopg2

from django.conf import settings

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'user_research.settings')

DATABASES = settings.DATABASES['default']

conn = psycopg2.connect(
    dbname=DATABASES['NAME'],
    user=DATABASES['USER'],
    password=DATABASES['PASSWORD'],
    host=DATABASES['HOST'],
    port=DATABASES['PORT'],
)

info = f"""
Connection Info:
---------------
Database Name: {DATABASES['NAME']}
User: {DATABASES['USER']}
Host: {DATABASES['HOST']}
Port: {DATABASES['PORT']}
Status: {'Connected' if conn.closed == 0 else 'Closed'}
"""

print(info)
conn.close()
