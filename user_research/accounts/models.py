from django.contrib.auth.base_user import BaseUserManager as DjangoBaseUserManager
from django.contrib.auth.models import AbstractUser
from django.core.validators import MaxLengthValidator
from django.db import models

from phonenumber_field.modelfields import PhoneNumberField

from accounts.validators import TelegramUsernameValidator
from core.validators import FileValidator


class BaseUserManager(DjangoBaseUserManager):
    """Define a model manager for User model with no username field."""

    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """Create and save a User with the given email and password."""
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        """Create and save a regular User with the given email and password."""
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        """Create and save a SuperUser with the given email and password."""
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **extra_fields)


class Role(models.TextChoices):
    RESPONDENT = 'respondent', 'Респондент'
    RESEARCHER = 'researcher', 'Исследователь'


class BaseUser(AbstractUser):
    """
    Extended base User model with no username field.

    Includes additional fields such as phone number, Whatsapp, and Telegram.
    A user can be either a researcher or a respondent, determined by
    the `role` field.
    """

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['role']

    objects = BaseUserManager()

    username = None
    email = models.EmailField('email address', unique=True)
    about_me = models.TextField(
        'about me',
        blank=True,
        validators=[MaxLengthValidator(1000)],
    )
    phone_number = PhoneNumberField('phone number', unique=True, blank=True, null=True)
    whatsapp = PhoneNumberField('whatsapp', unique=True, blank=True, null=True)
    telegram = models.CharField(
        'telegram',
        max_length=33,
        unique=True,
        blank=True,
        null=True,
        help_text=(
            'Username starts with an @ followed by a Latin letter. Latin letters, '
            'numbers, and underscores are allowed. Minimum length is 6 characters.'
        ),
        validators=[TelegramUsernameValidator()],
    )
    photo = models.ImageField(
        'photo',
        upload_to='users/photos/',
        blank=True,
        validators=[
            FileValidator(
                max_size=2 * 1024 * 1024, content_types=('image/jpeg', 'image/png')
            )
        ],
    )
    role = models.CharField('role', max_length=20, choices=Role.choices)

    # TODO(MVP2): Add relation with model Wallet.
    #  wallet = models.OneToOneField(Wallet, on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'user'
        verbose_name_plural = 'users'

    def __str__(self):
        return f'{self.email}'


class ProxyBaseUser(BaseUser):
    class Meta:
        proxy = True
        app_label = 'auth'
        verbose_name = 'user'
        verbose_name_plural = 'users'
