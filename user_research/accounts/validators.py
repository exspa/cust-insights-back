from django.core import validators
from django.core.exceptions import ValidationError
from django.utils.deconstruct import deconstructible
from rest_framework.validators import UniqueValidator


@deconstructible
class TelegramUsernameValidator(validators.RegexValidator):
    """
    Validates a Telegram username.

    Ensures the username starts with '@' followed by a Latin letter, and can
    include Latin letters, numbers, and underscores with a minimum length of
    6 characters.
    """

    regex = r'^@(?=\w{5,32}$)[a-zA-Z][a-zA-Z0-9]+(?:_[a-zA-Z0-9]+)*$'
    message = (
        'Username should start with @, followed by a Latin letter. '
        'Latin letters, numbers, and underscores are allowed.'
    )
    code = 'invalid_username'


class LatinCharactersPasswordValidator:
    def validate(self, password, user=None):
        if not all(ord(' ') <= ord(c) <= ord('~') for c in password):
            raise ValidationError(
                'The password must only contain Latin characters',
                code='non_latin_character',
            )

    def get_help_text(self):
        return 'Your password must only contain Latin characters'


class MaximumLengthPasswordValidator:
    def __init__(self, max_length=100):
        self.max_length = max_length

    def validate(self, password, user=None):
        if len(password) > self.max_length:
            raise ValidationError(
                'This password must not contain more than %(max_length)d characters.',
                code='password_too_long',
                params={'max_length': self.max_length},
            )

    def get_help_text(self):
        return 'Your password must not contain more than %(max_length)d characters.' % {
            'max_length': self.max_length
        }


class WhitespacePasswordValidator:
    def validate(self, password, user=None):
        if any(c.isspace() for c in password):
            raise ValidationError(
                'The password must not contain whitespace', code='password_whitespace'
            )

    def get_help_text(self):
        return 'Your password must not contain whitespace'


class LowerCaseCharacterPasswordValidator:
    def __init__(self, min_lower_chars=1):
        self.min_lower_chars = min_lower_chars

    def validate(self, password, user=None):
        if sum(c.islower() for c in password) < self.min_lower_chars:
            raise ValidationError(
                f'The password must contain at least {self.min_lower_chars} '
                f'lower case character(s).',
                code='lower_case_required',
            )

    def get_help_text(self):
        return (
            f'Your password must contain at least {self.min_lower_chars} '
            f'lower case character(s).'
        )


class UpperCaseCharacterPasswordValidator:
    def __init__(self, min_upper_chars=1):
        self.min_upper_chars = min_upper_chars

    def validate(self, password, user=None):
        if sum(c.isupper() for c in password) < self.min_upper_chars:
            raise ValidationError(
                f'The password must contain at least {self.min_upper_chars} '
                f'upper case character(s).',
                code='upper_case_required',
            )

    def get_help_text(self):
        return (
            f'Your password must contain at least {self.min_upper_chars} '
            f'upper case character(s).'
        )


class DigitCharacterPasswordValidator:
    def __init__(self, min_digit_chars=1):
        self.min_digit_chars = min_digit_chars

    def validate(self, password, user=None):
        if sum(c.isdigit() for c in password) < self.min_digit_chars:
            raise ValidationError(
                f'The password must contain at least {self.min_digit_chars} digit(s).',
                code='digit_required',
            )

    def get_help_text(self):
        return f'Your password must contain at least {self.min_digit_chars} digit(s).'


class SpecialCharacterPasswordValidator:
    def __init__(
        self,
        special_chars_set='!@#$%^&*(),.?":{}|<>-_/+=;[]\'\\`~',
        min_special_chars=1,
    ):
        self.special_chars_set = special_chars_set
        self.min_special_chars = min_special_chars

    def validate(self, password, user=None):
        if sum(c in self.special_chars_set for c in password) < self.min_special_chars:
            raise ValidationError(
                f'The password must contain at least {self.min_special_chars} '
                f'special character(s).',
                code='special_char_required',
            )

    def get_help_text(self):
        return (
            f'Your password must contain at least {self.min_special_chars} '
            f'special character(s).'
        )


class UserProfileUniqueValidator(UniqueValidator):
    """
    Custom validator for ensuring uniqueness of user profiles in cases where
    RespondentProfileSerializer inherits from UserProfileSerializer.

    It modifies the uniqueness check to focus on the user object instead of
    the respondent object.
    """

    def exclude_current_instance(self, queryset, instance):
        if instance is not None:
            instance = instance.user
        return super().exclude_current_instance(queryset, instance)
