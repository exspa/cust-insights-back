from django.db import transaction
from djoser.conf import settings as djoser_settings
from djoser.serializers import UserSerializer, UserCreateSerializer
from phonenumber_field.serializerfields import PhoneNumberField
from rest_framework import serializers

from .models import BaseUser as User
from .validators import TelegramUsernameValidator, UserProfileUniqueValidator
from researcher.models import ResearcherProfile
from respondent.models import RespondentProfile


class CustomUserSerializer(UserSerializer):
    password = serializers.CharField(write_only=True, trim_whitespace=True)

    class Meta(UserSerializer.Meta):
        fields = UserSerializer.Meta.fields + ('password',)


class CustomUserCreateSerializer(UserCreateSerializer):
    def perform_create(self, validated_data):
        """
        Create a new user and associated profile based on the provided role.
        """
        with transaction.atomic():
            user = User.objects.create_user(**validated_data)
            role = validated_data.get('role')

            if djoser_settings.SEND_ACTIVATION_EMAIL:
                user.is_active = False
                user.save(update_fields=['is_active'])

            if role == 'respondent':
                RespondentProfile.objects.create(user=user)
            elif role == 'researcher':
                ResearcherProfile.objects.create(user=user)

        return user


class UserProfileSerializer(serializers.Serializer):
    """
    Base serializer for common user profile fields in the
    respondent and researcher profiles.
    """

    contact_fields = ['phone_number', 'whatsapp', 'telegram']

    first_name = serializers.CharField(
        source='user.first_name', required=False, max_length=150
    )
    last_name = serializers.CharField(
        source='user.last_name', required=False, max_length=150
    )
    phone_number = PhoneNumberField(
        source='user.phone_number',
        required=False,
        validators=[UserProfileUniqueValidator(queryset=User.objects.all())],
    )
    whatsapp = PhoneNumberField(
        source='user.whatsapp',
        required=False,
        validators=[UserProfileUniqueValidator(queryset=User.objects.all())],
    )
    telegram = serializers.CharField(
        source='user.telegram',
        required=False,
        max_length=33,
        min_length=6,
        validators=[
            UserProfileUniqueValidator(queryset=User.objects.all()),
            TelegramUsernameValidator(),
        ],
    )

    photo = serializers.ImageField(source='user.photo', required=False)

    def update_user(self, user, user_data):
        # self.validate_contact_data(user_data)
        for field, value in user_data.items():
            setattr(user, field, value)
        user.save()

    # def validate_contact_data(self, user_data):
    #     """Check that at least one contact field is filled."""
    #   if not any(user_data.get(field) for field in self.contact_fields):
    #       raise serializers.ValidationError(
    #   {'contact_data': f'At least one of the following contact fields must '
    #                    f'be filled: {", ".join(self.contact_fields)}.'}
    #       )
