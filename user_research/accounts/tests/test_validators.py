import pytest

from django.contrib.auth.password_validation import validate_password
from django.core.exceptions import ValidationError

from accounts.validators import TelegramUsernameValidator
from . import test_data


class TestPasswordValidators:
    @pytest.mark.parametrize('password, expected_error_code', test_data.VALID_PASSWORDS)
    def test_valid_passwords(self, db, password, expected_error_code):
        assert validate_password(password) is None

    @pytest.mark.parametrize(
        'password, expected_error_code', test_data.INVALID_PASSWORDS
    )
    def test_invalid_passwords(self, db, password, expected_error_code):
        with pytest.raises(ValidationError) as exc_info:
            validate_password(password)
        assert exc_info.value.error_list[0].code == expected_error_code


class TestTelegramUsernameValidator:
    validator = TelegramUsernameValidator()

    @pytest.mark.parametrize('username', test_data.VALID_USERNAMES)
    def test_valid_telegram_usernames(self, username):
        assert self.validator(username) is None

    @pytest.mark.parametrize('username', test_data.INVALID_USERNAMES)
    def test_invalid_telegram_usernames(self, username):
        with pytest.raises(ValidationError) as exc_info:
            self.validator(username)
        assert exc_info.value.error_list[0].code == 'invalid_username'
