from faker import Faker

fake = Faker()
fake.seed_instance(1)

VALID_PASSWORDS = [
    ('dfjhhD1!', None),
    ('Te1St34!', None),
]

INVALID_PASSWORDS = [
    ('1qaz!QAZ', 'password_too_common'),
    ('dfjhD1!', 'password_too_short'),
    (fake.pystr(min_chars=101, max_chars=101, suffix='aZ9!'), 'password_too_long'),
    ('АбвЦуКепррррhD1!', 'non_latin_character'),
    ('龍門大酒家龍門大酒家hD1!', 'non_latin_character'),
    ('hD1!صسغذئآ', 'non_latin_character'),
    ('dfjho ryhD1!', 'password_whitespace'),
    ('FKFHDGRYGIOBVMDXBH1!', 'lower_case_required'),
    ('dfjhhd1!', 'upper_case_required'),
    ('fkxnsyrkgodjsnbskfdH!', 'digit_required'),
    ('fkxnsyrkgodjsnbskfdH1', 'special_char_required'),
]

VALID_USERNAMES = ['@username', '@user_name123', '@validUser1']

INVALID_USERNAMES = [
    'username',
    'user_name',
    '@user',
    '@123username',
    '@_username',
    '@username_',
]
