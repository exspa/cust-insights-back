from django.utils.translation import gettext_lazy as _

from rest_framework_simplejwt.authentication import JWTAuthentication
from rest_framework_simplejwt.exceptions import AuthenticationFailed, InvalidToken
from rest_framework_simplejwt.settings import api_settings


class CustomJWTAuthentication(JWTAuthentication):
    def get_user(self, validated_token):
        try:
            user_id = validated_token[api_settings.USER_ID_CLAIM]
        except KeyError:
            raise InvalidToken(_('Token contained no recognizable user identification'))

        try:
            user = (
                self.user_model.objects
                .select_related('respondent', 'researcher')
                .defer(
                    'researcher__website',
                    'researcher__logo',
                    'researcher__display_data_in_card',
                    'researcher__is_employee',
                    'researcher__user_id',
                    'respondent__about_me',
                    'respondent__date_of_birth',
                    'respondent__has_microphone',
                    'respondent__has_camera',
                    'respondent__has_experience',
                    'respondent__is_expert',
                    'respondent__user_id',
                    'respondent__gender_id',
                    'respondent__education_id',
                    'respondent__family_status_id',
                    'respondent__children_id',
                    'respondent__location_id',
                    'respondent__income_id',
                    'respondent__ownership_type_id',
                    'respondent__number_of_employees_id',
                    'respondent__position_id',
                    'respondent__business_area_id'
                )
                .get(**{api_settings.USER_ID_FIELD: user_id})
            )
        except self.user_model.DoesNotExist:
            raise AuthenticationFailed(_('User not found'), code='user_not_found')

        if not user.is_active:
            raise AuthenticationFailed(_('User is inactive'), code='user_inactive')

        return user
