import os
from django.http import JsonResponse
from core.utils import check_database


def health_check(request):
    """View function to check the health status of the service."""
    database_status = check_database()

    data = {
        'service': 'OK',
        'database': database_status,
        'version': os.environ.get('BUILD_VERSION', 'no_version'),
    }

    return JsonResponse(data)
