from functools import wraps

from django.core.exceptions import ImproperlyConfigured
from django.db import connection, connections, OperationalError


def check_database():
    """Utility function to check the connection to the default database."""
    try:
        connections['default'].cursor()
        return 'OK'
    except (OperationalError, ImproperlyConfigured):
        return 'Unavailable'


def count_queries(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        initial_queries = len(connection.queries)
        result = func(*args, **kwargs)
        final_queries = len(connection.queries)

        for query in connection.queries:
            print(query)
            print()

        print(
            f'All executed queries: {final_queries}',
            f'Queries executed by "{func.__name__}": {final_queries - initial_queries}',
            sep='\n'
        )
        return result

    return wrapper
