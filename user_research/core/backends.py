from django.contrib.auth import get_user_model
from django.contrib.auth.backends import ModelBackend

UserModel = get_user_model()


class CustomModelBackend(ModelBackend):
    def get_user(self, user_id):
        try:
            user = (
                UserModel._default_manager
                .select_related('respondent', 'researcher')
                .defer(
                    'researcher__website',
                    'researcher__logo',
                    'researcher__display_data_in_card',
                    'researcher__is_employee',
                    'researcher__user_id',
                    'respondent__about_me',
                    'respondent__date_of_birth',
                    'respondent__has_microphone',
                    'respondent__has_camera',
                    'respondent__has_experience',
                    'respondent__is_expert',
                    'respondent__user_id',
                    'respondent__gender_id',
                    'respondent__education_id',
                    'respondent__family_status_id',
                    'respondent__children_id',
                    'respondent__location_id',
                    'respondent__income_id',
                    'respondent__ownership_type_id',
                    'respondent__number_of_employees_id',
                    'respondent__position_id',
                    'respondent__business_area_id'
                )
                .get(pk=user_id)
            )
        except UserModel.DoesNotExist:
            return None
        return user if self.user_can_authenticate(user) else None
