from django.core.management.base import BaseCommand
from core.views import health_check
from django.http import HttpRequest


class Command(BaseCommand):
    """
    Django management command to run the health_check view and display its
    result in the console.
    """

    help = 'Run the health_check view and display the result in the console'

    def handle(self, *args, **kwargs):
        request = HttpRequest()
        response = health_check(request)
        self.stdout.write(response.content.decode())
