from django.db import transaction
from rest_framework import status
from rest_framework.exceptions import MethodNotAllowed, ValidationError
from rest_framework.response import Response


class M2MRelationManagerMixin:
    """
    Mixin to handle CRUD operations on many-to-many relationships
    for a given model instance.
    """

    def handle_get(self, field, serializer_class):
        items = field.all()
        serializer = serializer_class(items, many=True)
        return Response(serializer.data)

    def handle_post(self, request, serializer_class):
        serializer = serializer_class(data=request.data, many=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()

    @transaction.atomic()
    def handle_put(self, request, field, serializer_class):
        field.clear()
        serializer = serializer_class(data=request.data, many=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()

    def handle_delete(self, request, field, identifier_field, model_name):
        id_fields = self.get_fields_from_request(request, identifier_field)
        items = self.validate_items(field, id_fields, identifier_field, model_name)
        self.delete_items(field, items)
        return Response(status=status.HTTP_204_NO_CONTENT)

    def get_fields_from_request(self, request, identifier_field):
        data = request.data
        if not isinstance(data, list):
            raise ValidationError(
                {'detail': f'Expected a list of items but got type "{type(data)}"'}
            )
        id_fields = [i.get(identifier_field) for i in data if i.get(identifier_field)]

        if not id_fields:
            raise ValidationError({'detail': f'"{identifier_field}" is required.'})

        return id_fields

    def validate_items(self, field, id_fields, identifier_field, model_name):
        items = field.filter(**{f"{identifier_field}__in": id_fields})
        if len(items) != len(id_fields):
            div_fields = set(id_fields) - set(
                getattr(q, identifier_field) for q in items
            )
            raise ValidationError(
                {'detail': f'{model_name} {", ".join(map(str, div_fields))} not found.'}
            )
        return items

    def delete_items(self, field, items):
        if hasattr(field, 'remove'):
            field.remove(*items)
        else:
            items.delete()

    def handle_m2m_actions(
        self, instance, request, field_name, identifier_field, serializer_class
    ):
        field = getattr(instance, field_name)
        model_name = field.model.__name__

        if request.method == 'GET':
            return self.handle_get(field, serializer_class)
        elif request.method == 'POST':
            self.handle_post(request, serializer_class)
        elif request.method == 'PUT':
            self.handle_put(request, field, serializer_class)
        elif request.method == 'DELETE':
            return self.handle_delete(request, field, identifier_field, model_name)
        else:
            raise MethodNotAllowed(request.method)

        return self.handle_get(field, serializer_class)
