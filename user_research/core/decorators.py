def django_filter_warning(model):
    """
    This decorator is used to fix a warning in django-filter.
    See: https://github.com/carltongibson/django-filter/issues/966
    """

    def decorator(get_queryset_func):
        def wrapper(self):
            if getattr(self, 'swagger_fake_view', False):
                return model.objects.none()
            return get_queryset_func(self)
        return wrapper
    return decorator
