from django.db import transaction
from rest_framework import serializers

from .models import ResearcherProfile, CompanyData
from accounts.serializers import UserProfileSerializer


class ResearcherCompanyDataSerializer(serializers.Serializer):
    """
    Serializer for company data fields in the researcher profile.
    Provides methods to create and update employee data.
    """

    company_name = serializers.CharField(
        source='company_data.company_name', required=False
    )
    address = serializers.CharField(source='company_data.address', required=False)
    inn = serializers.CharField(source='company_data.inn', required=False)
    kpp = serializers.CharField(source='company_data.kpp', required=False)
    ogrn = serializers.CharField(source='company_data.ogrn', required=False)
    ogrnip = serializers.CharField(source='company_data.ogrnip', required=False)
    settlement_account = serializers.CharField(
        source='company_data.settlement_account', required=False
    )
    correspondent_account = serializers.CharField(
        source='company_data.correspondent_account', required=False
    )
    bik = serializers.CharField(source='company_data.bik', required=False)
    bank_name = serializers.CharField(source='company_data.bank_name', required=False)

    def create_company(self, researcher, validated_data):
        return CompanyData.objects.create(researcher=researcher, **validated_data)

    def update_company(self, company, validated_data):
        if company_data := validated_data.pop('company_data', None):
            for field, value in company_data.items():
                setattr(company, field, value)
            company.save()
        return company


class ResearcherProfileSerializer(
    UserProfileSerializer, ResearcherCompanyDataSerializer, serializers.ModelSerializer
):
    class Meta:
        model = ResearcherProfile
        exclude = ['user']

    @transaction.atomic
    def create(self, validated_data):
        user = self.context['request'].user
        company_data = validated_data.pop('company_data', {})
        validated_data = self.update_user_for_profile_creation(user, validated_data)
        researcher = super().create(validated_data)
        self.create_company(researcher, company_data)
        return researcher

    @transaction.atomic
    def update(self, instance, validated_data):
        self.update_user(instance.user, validated_data)
        self.update_company(instance.company_data, validated_data)
        return super().update(instance, validated_data)
