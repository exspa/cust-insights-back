from django.contrib import admin

from . import models


@admin.register(models.ResearcherProfile)
class ResearchAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'user',
        'is_employee',
        'company_data',
    )
    list_filter = ('is_employee',)
    search_fields = ('user__username', 'company__company_name')


@admin.register(models.CompanyData)
class CompanyDataAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'inn',
        'company_name',
        'address',
    )
    search_fields = ('company_name', 'address')
