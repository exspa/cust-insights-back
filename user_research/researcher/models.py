from django.core.exceptions import ValidationError
from django.core.validators import FileExtensionValidator
from django.db import models

from petrovna import validate_rs, validate_ks

from . import validators


class ResearcherProfile(models.Model):
    """
    ResearcherProfile model represents an individual researcher and
    their associated data.
    """

    website = models.URLField('Сайт', max_length=100, blank=True)
    logo = models.ImageField(
        'logo',
        upload_to='researchers/logos/',
        max_length=255,
        blank=True,
        validators=[
            FileExtensionValidator(allowed_extensions=['jpg']),
            # TODO: Add validators for size
        ],
    )

    display_data_in_card = models.BooleanField(
        'display data',
        default=False,
        help_text='Display the name, surname and photo in the researcher\'s card',
    )
    is_employee = models.BooleanField('status employee', default=False)

    user = models.OneToOneField(
        'accounts.BaseUser',
        on_delete=models.CASCADE,
        related_name='researcher',
        verbose_name='user',
    )

    class Meta:
        verbose_name = 'researcher'
        verbose_name_plural = 'researchers'

    def __str__(self):
        return f'{self.user.email}'


class CompanyData(models.Model):
    """
    CompanyData model represents the detailed information about
    a company or institution.
    """

    company_name = models.CharField(
        'company name',
        max_length=100,
        blank=True,
        validators=[],  # TODO: Add validators for company_name
    )
    address = models.CharField(
        'address',
        max_length=100,
        blank=True,
        validators=[],  # TODO: Add validators for address
    )
    inn = models.CharField(
        'inn',
        max_length=12,
        unique=True,
        blank=True,
        null=True,
        validators=[validators.validate_inn],
    )
    kpp = models.CharField(
        'kpp',
        max_length=9,
        blank=True,
        validators=[validators.validate_kpp],
    )
    ogrn = models.CharField(
        'ogrn',
        max_length=13,
        unique=True,
        blank=True,
        null=True,
        validators=[validators.validate_ogrn],
    )
    ogrnip = models.CharField(
        'ogrnip',
        max_length=15,
        unique=True,
        blank=True,
        null=True,
        validators=[validators.validate_ogrnip],
    )
    settlement_account = models.CharField(
        'settlement account',
        max_length=20,
        unique=True,
        blank=True,
        null=True,
    )
    correspondent_account = models.CharField(
        'correspondent account',
        max_length=20,
        blank=True,
    )
    bik = models.CharField(
        'bik',
        max_length=9,
        blank=True,
        validators=[validators.validate_bic],
    )
    bank_name = models.CharField(
        'bank name',
        max_length=100,
        blank=True,
        validators=[],  # TODO: Add validators for bank_name
    )

    researcher = models.OneToOneField(
        'ResearcherProfile',
        on_delete=models.CASCADE,
        related_name='company_data',
        verbose_name='researcher',
    )

    class Meta:
        verbose_name = 'company data'
        verbose_name_plural = 'company data'

    def __str__(self):
        return f'{self.researcher}'

    def clean(self):  # TODO: It may not work
        if not self.bik.strip():
            raise ValidationError('BIK field is required.')

        is_valid_rs, rs_error = validate_rs(
            self.settlement_account, self.bik, errors=True
        )
        if not is_valid_rs:
            raise ValidationError(rs_error)

        is_valid_ks, ks_error = validate_ks(
            self.correspondent_account, self.bik, errors=True
        )
        if not is_valid_ks:
            raise ValidationError(ks_error)
