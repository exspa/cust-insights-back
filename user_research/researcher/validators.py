import petrovna

from django.core import validators
from django.core.exceptions import ValidationError
from django.utils.deconstruct import deconstructible


def validate_inn(value):
    valid, error = petrovna.validate_inn(value, errors=True)
    if not valid:
        raise ValidationError(error)


def validate_kpp(value):
    valid, error = petrovna.validate_kpp(value, errors=True)
    if not valid:
        raise ValidationError(error)


def validate_ogrn(value):
    valid, error = petrovna.validate_ogrn(value, errors=True)
    if not valid:
        raise ValidationError(error)


def validate_ogrnip(value):
    valid, error = petrovna.validate_ogrnip(value, errors=True)
    if not valid:
        raise ValidationError(error)


def validate_settlement_account(value, bic):
    valid, error = petrovna.validate_rs(value, bic, errors=True)
    if not valid:
        raise ValidationError(error)


def validate_correspondent_account(value, bic):
    valid, error = petrovna.validate_ks(value, bic, errors=True)
    if not valid:
        raise ValidationError(error)


def validate_bic(value):
    valid, error = petrovna.validate_bic(value, errors=True)
    if not valid:
        raise ValidationError(error)


@deconstructible
class CyrillicValidator(validators.RegexValidator):
    """
    A validator that checks if a string contains only Cyrillic characters.
    """

    regex = r'^[а-яА-ЯёЁ\s]+$'
    message = 'The value should contain only cyrillic symbols.'
