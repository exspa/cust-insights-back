from rest_framework import permissions
from rest_framework.permissions import SAFE_METHODS

from accounts.models import Role


class IsResearcher(permissions.BasePermission):
    def has_permission(self, request, view):
        return (
            request.user.is_authenticated
            and request.user.role == Role.RESEARCHER
            and hasattr(request.user, 'researcher')
        )


class IsResearcherWithoutProfile(permissions.BasePermission):
    def has_permission(self, request, view):
        user = request.user
        return (
            user.is_authenticated
            and user.role == Role.RESEARCHER
            and not (hasattr(user, 'researcher') or hasattr(user, 'respondent'))
        )


class CurrentResearcher(IsResearcher):
    def has_object_permission(self, request, view, obj):
        researcher = request.user.researcher
        return obj == researcher


class CurrentResearcherOrAdmin(IsResearcher):
    def has_object_permission(self, request, view, obj):
        user = request.user
        researcher = request.user.researcher
        return user.is_staff or obj == researcher


class CurrentResearcherOrAdminOrReadOnly(IsResearcher):
    def has_object_permission(self, request, view, obj):
        user = request.user
        researcher = request.user.researcher
        if isinstance(obj, researcher) and obj == researcher:
            return True
        return request.method in SAFE_METHODS or user.is_staff
