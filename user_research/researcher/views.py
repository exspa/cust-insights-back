from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.exceptions import NotFound

from . import permissions
from .permissions import IsResearcherWithoutProfile
from .serializers import ResearcherProfileSerializer
from .models import ResearcherProfile
from core.decorators import django_filter_warning
from research.serializers import ResearchSerializer


class ResearcherViewSet(viewsets.ModelViewSet):
    http_method_names = [
        'get',
        'post',
        'put',
        'patch',
        'head',
        'options',
        'trace',
    ]

    serializer_class = ResearcherProfileSerializer
    queryset = ResearcherProfile.objects.select_related('user', 'company_data').all()
    permission_classes = [permissions.CurrentResearcherOrAdmin]

    def permission_denied(self, request, **kwargs):
        if request.user.is_authenticated and self.action in [
            'update',
            'partial_update',
            'list',
            'retrieve',
        ]:
            raise NotFound()
        super().permission_denied(request, **kwargs)

    @django_filter_warning(ResearcherProfile)
    def get_queryset(self):
        user = self.request.user
        queryset = super().get_queryset()
        if self.action == 'list' and not user.is_staff:
            queryset = queryset.filter(user=user)
        return queryset

    def get_permissions(self):
        if self.action == 'create':
            self.permission_classes = [IsResearcherWithoutProfile]
        elif self.action == 'list':
            self.permission_classes = [permissions.CurrentResearcher]
        elif self.action == 'me':
            self.permission_classes = [permissions.CurrentResearcher]
        elif self.action == 'destroy' or (
            self.action == 'me' and self.request and self.request.method == 'DELETE'
        ):
            self.permission_classes = [permissions.CurrentResearcherOrAdmin]
        return super().get_permissions()

    def get_serializer_class(self):
        if self.action == 'research' or self.action == 'me_research':
            return ResearchSerializer
        return self.serializer_class

    def get_current_researcher(self):
        return self.request.user.researcher

    def get_serializer_context(self):
        context = super().get_serializer_context()
        if hasattr(self.request.user, 'researcher'):
            context['researcher'] = self.get_current_researcher()
        return context

    @action(['get', 'put', 'patch'], detail=False)
    def me(self, request, *args, **kwargs):
        self.get_object = self.get_current_researcher
        if request.method == 'GET':
            return self.retrieve(request, *args, **kwargs)
        elif request.method == 'PUT':
            return self.update(request, *args, **kwargs)
        elif request.method == 'PATCH':
            return self.partial_update(request, *args, **kwargs)
