import re

from django.core.validators import RegexValidator
from django.db import transaction
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from . import models as respondent_models
from .models import Certificate, RespondentQualification
from accounts.serializers import UserProfileSerializer
from matching import models as matching_models
from matching import serializers as matching_serializers


class RespondentInterestAreaUpdateMixin:
    def update_interest_area(self, respondent, interest_area_data):
        interest_area_ids = {i['id'] for i in interest_area_data}
        self.validate_interest_area_data(interest_area_ids)
        respondent.interest_area.set(interest_area_ids)

    @staticmethod
    def validate_interest_area_data(interest_area_ids):
        matching_ids = set(
            matching_models.InterestArea.objects.filter(
                id__in=interest_area_ids
            ).values_list('id', flat=True)
        )
        diff_ids = interest_area_ids.difference(matching_ids)

        if diff_ids:
            raise serializers.ValidationError(
                {
                    'interest_area': [
                        f'Invalid pk "{pk}" - object does not exist.' for pk in diff_ids
                    ]
                }
            )


class RespondentBasicProfileSerializer(
    RespondentInterestAreaUpdateMixin,
    UserProfileSerializer,
    serializers.ModelSerializer,
):
    first_name = serializers.CharField(
        source='user.first_name',
        max_length=100,
        validators=[
            RegexValidator(
                r'^[A-Za-zА-Яа-я\-\s]+$',
                message='First name must contain only letters, spaces and hyphen.',
            )
        ],
    )
    last_name = serializers.CharField(
        source='user.last_name',
        max_length=100,
        validators=[
            RegexValidator(
                r'^[A-Za-zА-Яа-я\-\s]+$',
                message='Last name must contain only letters, spaces and hyphen.',
            )
        ],
    )

    gender_id = serializers.PrimaryKeyRelatedField(
        queryset=matching_models.Gender.objects.all(), source='gender', allow_null=True
    )
    education_id = serializers.PrimaryKeyRelatedField(
        queryset=matching_models.Education.objects.all(),
        source='education',
        allow_null=True,
    )
    family_status_id = serializers.PrimaryKeyRelatedField(
        queryset=matching_models.FamilyStatus.objects.all(),
        source='family_status',
        allow_null=True,
    )
    children_id = serializers.PrimaryKeyRelatedField(
        queryset=matching_models.Children.objects.all(),
        source='children',
        allow_null=True,
    )
    income_id = serializers.PrimaryKeyRelatedField(
        queryset=matching_models.Income.objects.all(), source='income', allow_null=True
    )
    location_id = serializers.PrimaryKeyRelatedField(
        queryset=matching_models.City.objects.all(), source='location', allow_null=True
    )

    interest_area = matching_serializers.InterestAreaSerializer(
        many=True, required=False
    )

    class Meta:
        model = respondent_models.RespondentProfile
        exclude = (
            'user',
            'gender',
            'education',
            'family_status',
            'children',
            'location',
            'income',
            'about_me',
            'has_microphone',
            'has_camera',
            'is_expert',
            'ownership_type',
            'number_of_employees',
            'position',
            'business_area',
            'device',
            'qualification',
        )
        extra_kwargs = {'date_of_birth': {'required': True, 'allow_null': False}}

    @transaction.atomic
    def update(self, instance, validated_data):
        if user_data := validated_data.pop('user', None):
            self.update_user(instance.user, user_data)

        if interest_area_data := validated_data.pop('interest_area', None):
            self.update_interest_area(instance, interest_area_data)

        return super().update(instance, validated_data)

    @classmethod
    def setup_eager_loading(cls, queryset, method):
        """Perform necessary eager loading of data."""
        queryset = queryset.select_related(
            'user',
        )
        queryset = queryset.defer(
            'has_microphone',
            'has_camera',
            'is_expert',
            'device',
            'qualification',
            'user__password',
            'user__last_login',
            'user__is_superuser',
            'user__is_staff',
            'user__is_active',
            'user__date_joined',
            'user__role',
        )

        return queryset


class RespondentDeviceUpdateMixin:
    def create_device(self, data):
        device_type, browser_ids, os_ids = self._device_key_from_data(data)
        device = matching_models.Device.objects.create(device_type=device_type)
        device.browser.set(browser_ids)
        device.operating_sys.set(os_ids)
        return device

    def update_device(self, respondent, device_data):
        """
        Create or retrieve a Device instance based on provided browser
        and operating system IDs.
        """
        all_devices = matching_models.Device.objects.prefetch_related(
            'browser', 'operating_sys'
        ).all()

        all_browser_ids = set()
        all_os_ids = set()
        device_cache = {}

        for device in all_devices:
            device_cache[self._device_key(device)] = device
            all_browser_ids.update(browser.id for browser in device.browser.all())
            all_os_ids.update(os.id for os in device.operating_sys.all())

        # self.validate_device_data(device_data, all_browser_ids, all_os_ids)

        devices = []

        for data in device_data:
            device_key = self._device_key_from_data(data)
            device = device_cache.get(device_key)

            if not device:
                device = self.create_device(data)
                device_cache[device_key] = device

            devices.append(device)

        respondent.device.set(devices)

    def validate_device_data(self, device_data, all_browser_ids, all_os_ids):
        errors = {}

        for data in device_data:
            device_type, browser_ids, os_ids = self._device_key_from_data(data)

            if diff_browser_ids := browser_ids.difference(all_browser_ids):
                errors.setdefault(device_type, []).append(
                    {
                        'browser': [
                            f'Invalid pk "{pk}" - object does not exist.'
                            for pk in diff_browser_ids
                        ]
                    }
                )

            if diff_os_ids := os_ids.difference(all_os_ids):
                errors.setdefault(device_type, []).append(
                    {
                        'operating_sys': [
                            f'Invalid pk "{pk}" - object does not exist.'
                            for pk in diff_os_ids
                        ]
                    }
                )

        if errors:
            raise ValidationError(errors)

    @staticmethod
    def _device_key(device):
        browser_ids = {browser.id for browser in device.browser.all()}
        os_ids = {os.id for os in device.operating_sys.all()}
        return device.device_type, frozenset(browser_ids), frozenset(os_ids)

    @staticmethod
    def _device_key_from_data(data):
        browser_ids = {i['id'] for i in data['browser']}
        os_ids = {i['id'] for i in data['operating_sys']}
        return data.get('device_type'), frozenset(browser_ids), frozenset(os_ids)


class RespondentAdvancedProfileSerializer(
    RespondentDeviceUpdateMixin, serializers.ModelSerializer
):
    ownership_type_id = serializers.PrimaryKeyRelatedField(
        queryset=matching_models.CompanyType.objects.all(),
        source='ownership_type',
        allow_null=True,
    )
    number_of_employees_id = serializers.PrimaryKeyRelatedField(
        queryset=matching_models.NumberOfEmployees.objects.all(),
        source='number_of_employees',
        allow_null=True,
    )
    position_id = serializers.PrimaryKeyRelatedField(
        queryset=matching_models.EmployeePosition.objects.all(),
        source='position',
        allow_null=True,
    )
    business_area_id = serializers.PrimaryKeyRelatedField(
        queryset=matching_models.BusinessArea.objects.all(),
        source='business_area',
        allow_null=True,
    )

    device = matching_serializers.DeviceSerializer(many=True, required=False)

    class Meta:
        model = respondent_models.RespondentProfile
        fields = (
            'about_me',
            'ownership_type_id',
            'number_of_employees_id',
            'position_id',
            'business_area_id',
            'has_microphone',
            'has_camera',
            'device',
        )

    @transaction.atomic
    def update(self, instance, validated_data):
        if device_data := validated_data.pop('device', None):
            self.update_device(instance, device_data)
        return super().update(instance, validated_data)

    @classmethod
    def setup_eager_loading(cls, queryset, method):
        """Perform necessary eager loading of data."""
        queryset = queryset.only(
            'has_microphone',
            'has_camera',
            'ownership_type',
            'number_of_employees',
            'position',
            'business_area',
        )

        if method == 'GET':
            queryset = queryset.prefetch_related(
                'device',
                'device__browser',
                'device__operating_sys',
            )

        return queryset


class RespondentExpertProfileSerializer(serializers.ModelSerializer):
    business_area = serializers.PrimaryKeyRelatedField(
        source='qualification.business_area', read_only=True
    )
    qualification = serializers.PrimaryKeyRelatedField(
        queryset=matching_models.BusinessArea.objects.all(),
        write_only=True,
    )
    uploaded_images = serializers.ListSerializer(
        child=serializers.FileField(),
        write_only=True,
    )

    class Meta:
        model = Certificate
        fields = '__all__'
        extra_kwargs = {'image': {'read_only': True}}

    def create(self, validated_data):
        qualification, created = RespondentQualification.objects.get_or_create(
            respondent=self.context['request'].user.respondent,
            business_area=validated_data.get('qualification'),
        )
        certificates = Certificate.objects.bulk_create(
            [
                Certificate(image=image, qualification=qualification)
                for image in validated_data.get('uploaded_images')
            ]
        )
        return certificates

    @classmethod
    def setup_eager_loading(cls, queryset, method=None):
        """Perform necessary eager loading of data."""
        queryset = queryset.select_related(
            'qualification',
            'qualification__respondent',
            'qualification__business_area',
        )
        return queryset


class RespondentProfileSerializer(UserProfileSerializer, serializers.ModelSerializer):
    gender = matching_serializers.GenderSerializer(read_only=True)
    education = matching_serializers.EducationSerializer(read_only=True)
    family_status = matching_serializers.FamilyStatusSerializer()
    children = matching_serializers.ChildrenSerializer(read_only=True)
    location = matching_serializers.CitySerializer(read_only=True)
    income = matching_serializers.IncomeSerializer(read_only=True)
    ownership_type = matching_serializers.CompanyTypeSerializer(read_only=True)
    number_of_employees = matching_serializers.NumberOfEmployeesSerializer(
        read_only=True
    )
    position = matching_serializers.EmployeePositionSerializer(read_only=True)
    business_area = matching_serializers.BusinessAreaSerializer(read_only=True)

    device = matching_serializers.DeviceSerializer(read_only=True, many=True)
    interest_area = matching_serializers.InterestAreaSerializer(
        read_only=True, many=True
    )
    qualification = matching_serializers.BusinessAreaSerializer(
        read_only=True, many=True
    )

    class Meta:
        model = respondent_models.RespondentProfile
        exclude = ('user',)

    @classmethod
    def setup_eager_loading(cls, queryset, method):
        """Perform necessary eager loading of data."""
        queryset = queryset.select_related(
            'user',
            'gender',
            'education',
            'family_status',
            'children',
            'location',
            'location__region',
            'location__region__country',
            'income',
            'ownership_type',
            'number_of_employees',
            'position',
            'business_area',
        )

        queryset = queryset.prefetch_related(
            'device',
            'device__browser',
            'device__operating_sys',
            'interest_area',
            'qualification',
        )

        queryset = queryset.defer(
            'user__password',
            'user__last_login',
            'user__is_superuser',
            'user__is_staff',
            'user__is_active',
            'user__date_joined',
            'user__role',
        )

        return queryset
