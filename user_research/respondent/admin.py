from django.contrib import admin

from matching.models import City, Device
from . import models


@admin.register(models.RespondentProfile)
class RespondentProfileAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'user',
        'has_experience',
        'is_expert',
    )
    list_display_links = ('id', 'user')
    list_filter = (
        'has_experience',
        'is_expert',
    )
    search_fields = ('user__username',)

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == 'location':
            kwargs['queryset'] = City.objects.select_related(
                'region', 'region__country'
            )
        return super().formfield_for_foreignkey(db_field, request, **kwargs)

    def formfield_for_manytomany(self, db_field, request, **kwargs):
        if db_field.name == 'device':
            kwargs['queryset'] = Device.objects.prefetch_related(
                'browser', 'operating_sys'
            )
        return super().formfield_for_manytomany(db_field, request, **kwargs)
