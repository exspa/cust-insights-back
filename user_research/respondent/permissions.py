from rest_framework import permissions
from accounts.models import Role


class IsRespondent(permissions.BasePermission):
    message = 'User is not a respondent'

    def has_permission(self, request, view):
        user = request.user
        return (
            user.is_authenticated
            and user.role == Role.RESPONDENT
        )


class CurrentRespondentOrAdmin(permissions.IsAuthenticated):

    def has_object_permission(self, request, view, obj):
        user = request.user
        respondent = request.user.respondent
        if (respondent is not None) and (obj == respondent):
            return True
        return user.is_staff
