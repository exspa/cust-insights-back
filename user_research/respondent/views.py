from rest_framework import mixins
from rest_framework.decorators import action
from rest_framework.exceptions import NotFound
from rest_framework.parsers import JSONParser
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from . import permissions
from . import serializers
from .models import RespondentProfile, Certificate
from .parsers import ArrayMultiPartParser
from core.decorators import django_filter_warning


class BaseRespondentProfileViewSet(GenericViewSet):
    queryset = RespondentProfile.objects.all()
    permission_classes = [permissions.CurrentRespondentOrAdmin]

    def permission_denied(self, request, **kwargs):
        if request.user.is_authenticated and self.action in [
            'update',
            'partial_update',
            'list',
            'retrieve',
        ]:
            raise NotFound()
        super().permission_denied(request, **kwargs)

    def get_permissions(self):
        if self.action == 'me':
            self.permission_classes = [permissions.IsRespondent]
        return super().get_permissions()

    @django_filter_warning(RespondentProfile)
    def get_queryset(self):
        user = self.request.user
        queryset = self.get_serializer_class().setup_eager_loading(
            super().get_queryset(), self.request.method
        )
        if self.action == 'list' and not user.is_staff:
            queryset = queryset.filter(user=user)
        return queryset

    def get_current_respondent(self):
        return self.get_queryset().get(user=self.request.user)

    @action(['get', 'put', 'patch'], detail=False)
    def me(self, request, *args, **kwargs):
        self.get_object = self.get_current_respondent
        if request.method == 'GET':
            return self.retrieve(request, *args, **kwargs)
        elif request.method == 'PUT':
            return self.update(request, *args, **kwargs)
        elif request.method == 'PATCH':
            return self.partial_update(request, *args, **kwargs)


class RespondentProfileViewSet(
    mixins.ListModelMixin, mixins.RetrieveModelMixin, BaseRespondentProfileViewSet
):
    serializer_class = serializers.RespondentProfileSerializer


class RespondentBasicProfileViewSet(
    mixins.RetrieveModelMixin, mixins.UpdateModelMixin, BaseRespondentProfileViewSet
):
    serializer_class = serializers.RespondentBasicProfileSerializer


class RespondentAdvancedProfileViewSet(RespondentBasicProfileViewSet):
    serializer_class = serializers.RespondentAdvancedProfileSerializer


class RespondentExpertProfileViewSet(BaseRespondentProfileViewSet):
    serializer_class = serializers.RespondentExpertProfileSerializer
    queryset = Certificate.objects.all()
    parser_classes = (JSONParser, ArrayMultiPartParser)

    @action(['get', 'post'], detail=False)
    def me(self, request, *args, **kwargs):
        certificates = self.get_queryset().filter(
            qualification__respondent=self.request.user.respondent
        )

        if request.method == 'POST':
            certificates.delete()
            serializer = self.get_serializer(data=request.data, many=True)
            serializer.is_valid(raise_exception=True)
            serializer.save()

        serializer = self.get_serializer(certificates, many=True)
        return Response(serializer.data)
