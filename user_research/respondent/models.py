from datetime import date

from django.core.validators import FileExtensionValidator, MaxLengthValidator
from django.db import models


class RespondentProfile(models.Model):
    """
    RespondentProfile model represents an individual respondent
    and their associated data.
    """

    about_me = models.TextField(
        'about me',
        blank=True,
        validators=[MaxLengthValidator(1000)],
    )

    date_of_birth = models.DateField('date of birth', blank=True, null=True)

    has_microphone = models.BooleanField('has microphone', default=False)
    has_camera = models.BooleanField('has camera', default=False)
    has_experience = models.BooleanField('has experience in research', default=False)
    is_expert = models.BooleanField('status expert', default=False)

    user = models.OneToOneField(
        'accounts.BaseUser',
        on_delete=models.CASCADE,
        related_name='respondent',
        verbose_name='user',
    )

    gender = models.ForeignKey(
        'matching.Gender',
        on_delete=models.SET_NULL,
        related_name='respondents',
        verbose_name='gender',
        blank=True,
        null=True,
    )
    education = models.ForeignKey(
        'matching.Education',
        on_delete=models.SET_NULL,
        related_name='respondents',
        verbose_name='education',
        blank=True,
        null=True,
    )
    family_status = models.ForeignKey(
        'matching.FamilyStatus',
        on_delete=models.SET_NULL,
        related_name='respondents',
        verbose_name='family status',
        blank=True,
        null=True,
    )
    children = models.ForeignKey(
        'matching.Children',
        on_delete=models.SET_NULL,
        related_name='respondents',
        verbose_name='children',
        blank=True,
        null=True,
    )
    location = models.ForeignKey(
        'matching.City',
        on_delete=models.SET_NULL,
        related_name='respondents',
        verbose_name='location',
        blank=True,
        null=True,
    )
    income = models.ForeignKey(
        'matching.Income',
        on_delete=models.SET_NULL,
        related_name='respondents',
        verbose_name='income',
        blank=True,
        null=True,
    )
    ownership_type = models.ForeignKey(
        'matching.CompanyType',
        on_delete=models.SET_NULL,
        related_name='respondents',
        verbose_name='ownership type of the company',
        null=True,
    )
    number_of_employees = models.ForeignKey(
        'matching.NumberOfEmployees',
        on_delete=models.SET_NULL,
        related_name='respondents',
        verbose_name='number of employees',
        null=True,
    )
    position = models.ForeignKey(
        'matching.EmployeePosition',
        on_delete=models.SET_NULL,
        related_name='respondents',
        verbose_name='employee\'s position',
        null=True,
    )
    business_area = models.ForeignKey(
        'matching.BusinessArea',
        on_delete=models.SET_NULL,
        related_name='respondents',
        verbose_name='business area',
        null=True,
    )

    device = models.ManyToManyField(
        'matching.Device',
        related_name='respondents',
        verbose_name='device',
        blank=True,
    )

    interest_area = models.ManyToManyField(
        'matching.InterestArea',
        related_name='respondents',
        verbose_name='interest area',
        blank=True,
    )
    qualification = models.ManyToManyField(
        'matching.BusinessArea',
        related_name='qualification_respondents',
        through='RespondentQualification',
        verbose_name='qualification',
        blank=True,
    )

    class Meta:
        verbose_name = 'respondent'
        verbose_name_plural = 'respondents'

    def __str__(self):
        return f'{self.user.email}'

    def age(self):
        """
        Calculate the age of the respondent based on the date of birth.

        Returns:
            int: Age of the respondent.
        """
        if not self.date_of_birth:
            return

        today = date.today()
        birth_date = self.date_of_birth
        year_diff = today.year - birth_date.year
        has_had_birthday = (today.month, today.day) >= (
            birth_date.month,
            birth_date.day,
        )
        age = year_diff if has_had_birthday else year_diff - 1
        return age


class Certificate(models.Model):
    """
    Certificate model represents a certification associated
    with a respondent's qualification.
    """

    image = models.FileField(
        'certificate image',
        upload_to='respondents/certificates/',
        max_length=255,
        validators=[
            FileExtensionValidator(allowed_extensions=['jpg', 'pdf']),
            # TODO: Add validators for size
        ],
    )

    qualification = models.ForeignKey(
        'RespondentQualification',
        on_delete=models.CASCADE,
        related_name='certificates',
        verbose_name='qualification',
    )

    class Meta:
        verbose_name = 'certificate'
        verbose_name_plural = 'certificates'

    def __str__(self):
        return (
            f'{self.qualification.respondent}, ' f'{self.qualification.business_area}'
        )


class RespondentQualification(models.Model):
    """
    RespondentQualification model represents specific qualifications
    possessed by a respondent.
    """

    respondent = models.ForeignKey(
        'RespondentProfile',
        on_delete=models.CASCADE,
        related_name='respondent_qualifications',
        verbose_name='respondent',
    )
    business_area = models.ForeignKey(
        'matching.BusinessArea',
        on_delete=models.CASCADE,
        related_name='respondent_qualifications',
        verbose_name='business area',
    )

    class Meta:
        unique_together = ['respondent', 'business_area']
        verbose_name = 'qualification of the respondent'
        verbose_name_plural = 'qualifications of respondents'

    def __str__(self):
        return f'{self.respondent}, {self.business_area}'
