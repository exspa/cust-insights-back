import re

from django.conf import settings
from django.http.multipartparser import MultiPartParser as DjangoMultiPartParser
from django.http.multipartparser import MultiPartParserError

from rest_framework.exceptions import ParseError
from rest_framework.parsers import BaseParser, DataAndFiles


class ArrayMultiPartParser(BaseParser):
    """
    Parser for multipart form data, which may include file data and
    convert into array.

    This parser is designed to handle multipart form data where keys
    can represent arrays.
    Each key for an array element should be suffixed with '[index]', where
    'index' is the position in the array.

    For example, to represent two items in an array, the form data keys would
    be structured as follows:
    - 'business_area[0]': 'IT'
    - 'uploaded_images[0]': (file for the first item)
    - 'uploaded_images[0]': (file for the first item)
    - 'business_area[1]': 'finance'
    - 'uploaded_images[1]': (file for the second item)

    The parser will group data and files based on these indices, constructing
    an array of dictionaries, each containing the data and files for a
    specific index.

    Note: If no indices are detected, the parser will default to handling
    the request as a standard multipart form request without array conversion.
    """

    media_type = 'multipart/form-data'

    def parse(self, stream, media_type=None, parser_context=None):
        parser_context = parser_context or {}
        request = parser_context['request']
        encoding = parser_context.get('encoding', settings.DEFAULT_CHARSET)
        meta = request.META.copy()
        meta['CONTENT_TYPE'] = media_type
        upload_handlers = request.upload_handlers

        try:
            parser = DjangoMultiPartParser(meta, stream, upload_handlers, encoding)
            data, files = parser.parse()

            index_pattern = re.compile(r'\[(\d+)]')

            indexes = set()
            for key in list(data.keys()) + list(files.keys()):
                match = index_pattern.search(key)
                if match:
                    indexes.add(match.group(1))

            if not indexes:
                return DataAndFiles(data, files)

            data_list = []
            for index in sorted(indexes, key=int):
                entry = {}

                for key, value in data.items():
                    if f'[{index}]' in key:
                        new_key = key.split('[')[0]
                        entry[new_key] = value

                for key, uploaded_files in files.lists():
                    if f'[{index}]' in key:
                        new_key = key.split('[')[0]
                        entry[new_key] = uploaded_files

                data_list.append(entry)

            return DataAndFiles(data_list, files)
        except MultiPartParserError as exc:
            raise ParseError('Multipart form parse error - %s' % str(exc))
