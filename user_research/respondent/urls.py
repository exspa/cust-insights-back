from rest_framework import routers

from . import views


router = routers.SimpleRouter()
router.register(r'respondent', views.RespondentProfileViewSet, basename='respondent')
router.register(r'respondent/basic', views.RespondentBasicProfileViewSet, basename='respondent')
router.register(r'respondent/advanced', views.RespondentAdvancedProfileViewSet, basename='respondent')
router.register(r'respondent/expert', views.RespondentExpertProfileViewSet, basename='respondent')

urlpatterns = router.urls
