from django.contrib import admin

from . import models


@admin.register(models.Research)
class ResearchAdmin(admin.ModelAdmin):
    list_display = ('id', 'author', 'title')
    list_display_links = ('id', 'title')
    search_fields = ['author', 'title']
