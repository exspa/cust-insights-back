from rest_framework import serializers
from drf_writable_nested import WritableNestedModelSerializer

from matching import serializers as matching_serializer
from matching.models import ResearchType
from research.models import Research


class CurrentAuthorDefault(serializers.CurrentUserDefault):
    def __call__(self, serializer_field):
        return serializer_field.context['author']


class ResearchSerializer(WritableNestedModelSerializer):
    author = serializers.HiddenField(default=CurrentAuthorDefault())
    research_type = serializers.SlugRelatedField(
        queryset=ResearchType.objects.all(),
        slug_field='title',
    )
    device = matching_serializer.DeviceSerializer(many=True, required=False)
    gender = matching_serializer.GenderSerializer(many=True, required=False)
    education = matching_serializer.EducationSerializer(many=True, required=False)
    city = matching_serializer.CitySerializer(many=True, required=False)
    region = matching_serializer.RegionSerializer(many=True, required=False)
    income = matching_serializer.IncomeSerializer(many=True, required=False)
    children = matching_serializer.ChildrenSerializer(many=True, required=False)
    family_status = matching_serializer.FamilyStatusSerializer(
        many=True, required=False
    )
    interests_area = matching_serializer.InterestAreaSerializer(
        many=True, required=False
    )
    company_type = matching_serializer.CompanyTypeSerializer(many=True, required=False)
    number_of_employees = matching_serializer.NumberOfEmployeesSerializer(
        many=True, required=False
    )
    employee_position = matching_serializer.EmployeePositionSerializer(
        many=True, required=False
    )
    business_area = matching_serializer.BusinessAreaSerializer(
        many=True, required=False
    )

    class Meta:
        model = Research
        exclude = ['created_at', 'update_at']
