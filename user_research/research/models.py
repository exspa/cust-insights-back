from django.core.validators import MaxLengthValidator
from django.db import models

RESEARCH_DEFAULT_TITLE = 'Новый проект'


class ResearchStatus(models.TextChoices):
    DRAFT = 'draft', 'черновик'
    PENDING = 'pending', 'ожидание публикации'
    CANCELED = 'canceled', 'отменено'
    PUBLISHED = 'published', 'опубликовано'
    FINISH = 'finish', 'завершено'


class ResearchTime(models.TextChoices):
    TIME_15 = '15m', '15 минут'
    TIME_30 = '30m', '30 минут'
    TIME_45 = '45m', '45 минут'
    TIME_60 = '60m', '1 час'


class Research(models.Model):
    """
    Model representing a research study.

    This model captures details about a research, including its title,
    description, status, duration, and other related attributes. It also
    establishes relationships with other models such as the researcher's
    profile, the type of research, devices used by respondents,
    and various demographic and professional criteria of respondents.
    """

    title = models.CharField(
        'title research',
        max_length=100,
        default=RESEARCH_DEFAULT_TITLE,
    )
    public_title = models.CharField('public title', max_length=100)
    description = models.TextField(
        'description',
        validators=[MaxLengthValidator(500)],
    )
    status = models.CharField(
        'status',
        max_length=20,
        choices=ResearchStatus.choices,
        default=ResearchStatus.DRAFT,
    )

    research_time = models.CharField(
        'research time',
        max_length=3,
        choices=ResearchTime.choices,
    )

    number_of_question = models.PositiveSmallIntegerField(
        verbose_name='number of question'
    )
    respondent_count = models.PositiveSmallIntegerField(
        verbose_name='number of respondents'
    )
    age_from = models.PositiveSmallIntegerField('minimum age', null=True, blank=True)
    age_to = models.PositiveSmallIntegerField('maximum age', null=True, blank=True)

    created_at = models.DateTimeField('date created', auto_now_add=True)
    update_at = models.DateTimeField('date updated', auto_now=True)

    has_experience = models.BooleanField('has experience in research', default=False)
    has_microphone = models.BooleanField('has microphone', default=False)
    has_camera = models.BooleanField('has camera', default=False)
    only_experts = models.BooleanField('status expert', default=False)
    is_employee = models.BooleanField('status employee', default=False)

    author = models.ForeignKey(
        'researcher.ResearcherProfile',
        on_delete=models.PROTECT,
        related_name='research',
        verbose_name='author',
    )
    research_type = models.ForeignKey(
        'matching.ResearchType',
        on_delete=models.PROTECT,
        related_name='researches',
        verbose_name='type',
    )
    research_team = models.ManyToManyField(
        'researcher.ResearcherProfile',
        related_name='researches_team',
        verbose_name='team',
        blank=True,
    )
    device = models.ManyToManyField(
        'matching.Device',
        related_name='researches',
        verbose_name='device',
        blank=True,
    )
    gender = models.ManyToManyField(
        'matching.Gender',
        related_name='researches',
        verbose_name='respondent\'s gender ',
        blank=True,
    )
    education = models.ManyToManyField(
        'matching.Education',
        related_name='researches',
        verbose_name='respondent education',
        blank=True,
    )
    family_status = models.ManyToManyField(
        'matching.FamilyStatus',
        related_name='researches',
        verbose_name='respondent family status',
        blank=True,
    )
    children = models.ManyToManyField(
        'matching.Children',
        related_name='researches',
        verbose_name='amount of children',
        blank=True,
    )
    city = models.ManyToManyField(
        'matching.City',
        related_name='researches',
        verbose_name='respondent\'s city',
        blank=True,
    )
    region = models.ManyToManyField(
        'matching.Region',
        related_name='researches',
        verbose_name='respondent\'s region',
        blank=True,
    )
    income = models.ManyToManyField(
        'matching.Income',
        related_name='researches',
        verbose_name='respondent\'s monthly income',
        blank=True,
    )
    interests_area = models.ManyToManyField(
        'matching.InterestArea',
        related_name='researches',
        verbose_name='respondent\'s areas of interest',
        blank=True,
    )
    company_type = models.ManyToManyField(
        'matching.CompanyType',
        related_name='researches',
        verbose_name='company ownership types',
        blank=True,
    )
    number_of_employees = models.ManyToManyField(
        'matching.NumberOfEmployees',
        related_name='researches',
        verbose_name='number of employees',
        blank=True,
    )
    employee_position = models.ManyToManyField(
        'matching.EmployeePosition',
        related_name='researches',
        verbose_name='respondent\'s position',
        blank=True,
    )
    business_area = models.ManyToManyField(
        'matching.BusinessArea',
        related_name='researches',
        verbose_name='company\'s business areas',
        blank=True,
    )

    class Meta:
        verbose_name = 'research'
        verbose_name_plural = 'research'
        ordering = ['-created_at']

    def __str__(self):
        return f'{self.title[:20]}, {self.author}'
