from rest_framework import viewsets
from rest_framework.exceptions import NotFound

from . import serializers, permissions
from .models import Research
from core.decorators import django_filter_warning
from researcher.permissions import IsResearcher


class ResearchViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.ResearchSerializer
    queryset = Research.objects.select_related().all()
    permission_classes = [permissions.IsAuthorOrAdmin]
    filterset_fields = ['status', 'research_type__title', 'gender__title']

    def permission_denied(self, request, **kwargs):
        if request.user.is_authenticated and self.action in [
            'update',
            'partial_update',
            'list',
            'retrieve',
        ]:
            raise NotFound()
        super().permission_denied(request, **kwargs)

    @django_filter_warning(Research)
    def get_queryset(self):
        user = self.request.user
        queryset = super().get_queryset()
        if self.action == 'list' and not user.is_staff:
            researcher = self.get_author()
            queryset = queryset.filter(author=researcher)
        return queryset

    def get_permissions(self):
        if self.action == 'create':
            self.permission_classes = [IsResearcher]
        elif self.action == 'list':
            self.permission_classes = [permissions.IsAuthorOrAdmin]
        elif self.action == 'destroy':
            self.permission_classes = [permissions.IsAuthorOrAdmin]
        return super().get_permissions()

    def get_author(self):
        return self.request.user.researcher

    def get_serializer_context(self):
        context = super().get_serializer_context()
        if hasattr(self.request.user, 'researcher'):
            context['author'] = self.get_author()
        return context
