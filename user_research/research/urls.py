from rest_framework import routers

from . import views


router = routers.SimpleRouter()
router.register(r'research', views.ResearchViewSet, basename='research')

urlpatterns = router.urls
