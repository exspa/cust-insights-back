from rest_framework.permissions import SAFE_METHODS

from researcher.permissions import IsResearcher


class IsAuthor(IsResearcher):
    def has_object_permission(self, request, view, obj):
        researcher = request.user.researcher
        return obj.author == researcher


class IsAuthorOrAdmin(IsResearcher):
    def has_object_permission(self, request, view, obj):
        user = request.user
        researcher = request.user.researcher
        return user.is_staff or obj.author == researcher


class IsAuthorOrAdminOrReadOnly(IsResearcher):
    def has_object_permission(self, request, view, obj):
        user = request.user
        researcher = request.user.researcher
        if isinstance(obj.author, researcher) and obj.author == researcher:
            return True
        return request.method in SAFE_METHODS or user.is_staff
