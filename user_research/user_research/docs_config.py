from django.conf import settings
from rest_framework.permissions import AllowAny

from drf_yasg import openapi
from drf_yasg.views import get_schema_view


DESCRIPTION_API = """The CustInsights backend API is designed to automate
the qualitative research process with respondents in the Russian market for
businesses. This system aims to streamline the research procedure, making
it efficient and accurate for business enterprises."""


schema_view = get_schema_view(
    openapi.Info(
        title='CustInsights API',
        default_version='v1',
        description=DESCRIPTION_API,
        terms_of_service='https://custinsights.ru/terms/',
        contact=openapi.Contact(email='support@custinsights.com'),
        license=openapi.License(name='BSD License'),
    ),
    url=f'{settings.PROTOCOL}://{settings.DOMAIN}',
    public=True,
    permission_classes=[AllowAny],
)
