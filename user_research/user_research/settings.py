import os
from datetime import timedelta
from pathlib import Path

import environ

env = environ.Env()

# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent

# Take environment variables from .env file
environ.Env.read_env(os.path.join(BASE_DIR, '.env'))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/4.2/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
# print(os.environ.get('SECRET_KEY'))
SECRET_KEY = env.str('SECRET_KEY')

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = env.bool('DEBUG', default=False)

ALLOWED_HOSTS = env.list('ALLOWED_HOSTS', default=['*'])

# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'rest_framework',
    'rest_framework.authtoken',
    'rest_framework_simplejwt',
    'rest_framework_simplejwt.token_blacklist',

    'corsheaders',
    'djoser',
    'drf_yasg',
    'debug_toolbar',
    'django_filters',
    'minio_storage',
    'phonenumber_field',
    'whitenoise.runserver_nostatic',

    'accounts.apps.AccountsConfig',
    'core.apps.CoreConfig',
    'interview.apps.InterviewConfig',
    'matching.apps.MatchingConfig',
    'polls.apps.PollsConfig',
    'research.apps.ResearchConfig',
    'researcher.apps.ResearcherConfig',
    'respondent.apps.RespondentConfig',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'whitenoise.middleware.WhiteNoiseMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'debug_toolbar.middleware.DebugToolbarMiddleware',
]

AUTHENTICATION_BACKENDS = [
    'core.backends.CustomModelBackend',
]

ROOT_URLCONF = 'user_research.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'user_research.wsgi.application'


# Database
# https://docs.djangoproject.com/en/4.2/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': env.str('POSTGRESQL_DB', default='postgres'),
        'USER': env.str('POSTGRESQL_USER', default='postgres'),
        'PASSWORD': env.str('POSTGRESQL_PASSWORD', default='pass'),
        'HOST': env.str('POSTGRESQL_HOST', default='db'),
        'PORT': env.str('POSTGRESQL_PORT', default='5432'),
    }
}


# Password validation
# https://docs.djangoproject.com/en/4.2/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator'},
    {'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator'},
    {'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator'},
    {'NAME': 'accounts.validators.MaximumLengthPasswordValidator'},
    {'NAME': 'accounts.validators.LatinCharactersPasswordValidator'},
    {'NAME': 'accounts.validators.WhitespacePasswordValidator'},
    {'NAME': 'accounts.validators.LowerCaseCharacterPasswordValidator'},
    {'NAME': 'accounts.validators.UpperCaseCharacterPasswordValidator'},
    {'NAME': 'accounts.validators.DigitCharacterPasswordValidator'},
    {'NAME': 'accounts.validators.SpecialCharacterPasswordValidator'},
]


# Internationalization
# https://docs.djangoproject.com/en/4.2/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/4.2/howto/static-files/

MEDIA_URL = env.str('MEDIA_URL', default='media/')
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

STATIC_URL = env.str('STATIC_URL', default='static/')
STATIC_ROOT = os.path.join(BASE_DIR, 'static')

if not DEBUG:
    DEFAULT_FILE_STORAGE = 'minio_storage.storage.MinioMediaStorage'
    STATICFILES_STORAGE = 'minio_storage.storage.MinioStaticStorage'

# Minio
MINIO_STORAGE_ENDPOINT = 's3.custinsights.ru'
MINIO_STORAGE_USE_HTTPS = True
MINIO_STORAGE_AUTO_CREATE_MEDIA_BUCKET = True
MINIO_STORAGE_ASSUME_MEDIA_BUCKET_EXISTS = True
MINIO_STORAGE_ACCESS_KEY = env.str('MINIO_STORAGE_ACCESS_KEY', default=None)
MINIO_STORAGE_SECRET_KEY = env.str('MINIO_STORAGE_SECRET_KEY', default=None)
MINIO_STORAGE_MEDIA_BUCKET_NAME = env.str('MINIO_STORAGE_MEDIA_BUCKET_NAME', default='backend-dev-media')
MINIO_STORAGE_STATIC_BUCKET_NAME = env.str('MINIO_STORAGE_STATIC_BUCKET_NAME', default='backend-dev-static')


# Default primary key field type
# https://docs.djangoproject.com/en/4.2/ref/settings/#default-auto-field

DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'


# Common django settings

PROTOCOL = 'https' if not DEBUG else 'http'
DOMAIN = env.str('DOMAIN', default='api.custinsights.ru')
SITE_NAME = env.str('SITE_NAME', default='Cust Insights')

EMAIL_BACKEND = env.str(
    'EMAIL_BACKEND', default='django.core.mail.backends.smtp.EmailBackend'
)
EMAIL_HOST_USER = env.str('EMAIL_HOST_USER', default=None)
EMAIL_HOST_PASSWORD = env.str('EMAIL_HOST_PASSWORD', default=None)
EMAIL_HOST = 'smtp.yandex.ru'
EMAIL_USE_SSL = True
EMAIL_PORT = 465

DEFAULT_FROM_EMAIL = env.str('EMAIL', default=None)

AUTH_USER_MODEL = 'accounts.BaseUser'

FILE_UPLOAD_MAX_MEMORY_SIZE = 5242880


# Security

CSRF_TRUSTED_ORIGINS = env.list('CSRF_TRUSTED_ORIGINS', default=['https://*.custinsights.ru'])
CSRF_COOKIE_SECURE = env.bool('CSRF_COOKIE_SECURE', default=True)
CORS_ALLOW_ALL_ORIGINS = env.bool('CORS_ALLOW_ALL_ORIGINS', default=True)
SESSION_COOKIE_SECURE = env.bool('SESSION_COOKIE_SECURE', default=True)


# Django Rest Framework

REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.SessionAuthentication',
        'core.authentication.CustomJWTAuthentication',
    ),
    'DEFAULT_FILTER_BACKENDS': ('django_filters.rest_framework.DjangoFilterBackend', ),
    'DEFAULT_PARSER_CLASSES': (
        'rest_framework.parsers.JSONParser',
        'rest_framework.parsers.MultiPartParser',
    ),
    'DEFAULT_RENDERER_CLASSES': env.list(
        'DEFAULT_RENDERER_CLASSES', default=['rest_framework.renderers.JSONRenderer']
    )
}


# Djoser

DJOSER = {
    'PASSWORD_RESET_CONFIRM_URL': '#/password/reset/confirm/{uid}/{token}',
    'ACTIVATION_URL': '#/activate/{uid}/{token}',
    'SEND_ACTIVATION_EMAIL': False,
    'SERIALIZERS': {
        'user': 'accounts.serializers.CustomUserSerializer',
        'current_user': 'accounts.serializers.CustomUserSerializer',
        'user_create': 'accounts.serializers.CustomUserCreateSerializer',
    },
}


# Simple JWT

SIMPLE_JWT = {
    'ACCESS_TOKEN_LIFETIME': timedelta(days=1),  # minutes=15
    'REFRESH_TOKEN_LIFETIME': timedelta(days=1),
    'ROTATE_REFRESH_TOKENS': True,
    'BLACKLIST_AFTER_ROTATION': True,
    'UPDATE_LAST_LOGIN': True,

    'ALGORITHM': 'HS256',
    'SIGNING_KEY': env.str('SECRET_KEY'),
    'VERIFYING_KEY': '',
    'AUDIENCE': None,
    'ISSUER': None,
    'JSON_ENCODER': None,
    'JWK_URL': None,
    'LEEWAY': 0,

    'AUTH_HEADER_TYPES': ('Bearer',),
    'AUTH_HEADER_NAME': 'HTTP_AUTHORIZATION',
    'USER_ID_FIELD': 'id',
    'USER_ID_CLAIM': 'user_id',
    'USER_AUTHENTICATION_RULE': 'rest_framework_simplejwt.authentication.default_user_authentication_rule',

    'AUTH_TOKEN_CLASSES': ('rest_framework_simplejwt.tokens.AccessToken',),
    'TOKEN_TYPE_CLAIM': 'token_type',
    'TOKEN_USER_CLASS': 'rest_framework_simplejwt.models.TokenUser',

    'JTI_CLAIM': 'jti',

    'SLIDING_TOKEN_REFRESH_EXP_CLAIM': 'refresh_exp',
    'SLIDING_TOKEN_LIFETIME': timedelta(minutes=5),
    'SLIDING_TOKEN_REFRESH_LIFETIME': timedelta(days=1),

    'TOKEN_OBTAIN_SERIALIZER': 'rest_framework_simplejwt.serializers.TokenObtainPairSerializer',
    'TOKEN_REFRESH_SERIALIZER': 'rest_framework_simplejwt.serializers.TokenRefreshSerializer',
    'TOKEN_VERIFY_SERIALIZER': 'rest_framework_simplejwt.serializers.TokenVerifySerializer',
    'TOKEN_BLACKLIST_SERIALIZER': 'rest_framework_simplejwt.serializers.TokenBlacklistSerializer',
    'SLIDING_TOKEN_OBTAIN_SERIALIZER': 'rest_framework_simplejwt.serializers.TokenObtainSlidingSerializer',
    'SLIDING_TOKEN_REFRESH_SERIALIZER': 'rest_framework_simplejwt.serializers.TokenRefreshSlidingSerializer',
}


# ReDoc

REDOC_SETTINGS = {
   'LAZY_RENDERING': False,
}

# Debug Toolbar

INTERNAL_IPS = ['127.0.0.1']
