from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include

from core.views import health_check
from .docs_config import schema_view


urlpatterns = [
    path('admin/', admin.site.urls),
    path('swagger/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui',),
    path('redoc/', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
    path('ht/', health_check, name='health_check'),
    path('', include('accounts.urls')),
    path('', include('matching.urls')),
    path('', include('respondent.urls')),
    path('', include('researcher.urls')),
    path('', include('research.urls')),
    path('', include('polls.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    urlpatterns += [path('__debug__/', include('debug_toolbar.urls'))]
