from rest_framework.permissions import IsAuthenticated

from research.models import Research


class IsResearchAuthor(IsAuthenticated):
    message = 'Research does not belong to the current user.'

    def has_permission(self, request, view):
        is_authenticated = super().has_permission(request, view)
        if not is_authenticated:
            return False

        if view.action in ['create']:
            research_id = request.data.get('research')
            researcher = request.user.researcher
            return Research.objects.filter(id=research_id, author=researcher).exists()
        return True

    def has_object_permission(self, request, view, obj):
        if view.action == 'destroy':
            return obj.research.author == request.user.researcher
        return True
