from rest_framework import routers

from .views import PollViewSet

router = routers.SimpleRouter()
router.register(r'poll', PollViewSet, basename='poll')

urlpatterns = router.urls
