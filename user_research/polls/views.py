from django.db import transaction
from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.exceptions import NotFound
from rest_framework.response import Response

from . import serializers
from .permissions import IsResearchAuthor
from .models import Poll
from core.decorators import django_filter_warning
from core.mixins import M2MRelationManagerMixin
from researcher.permissions import IsResearcher
from respondent.permissions import IsRespondent


class PollViewSet(M2MRelationManagerMixin, viewsets.ModelViewSet):
    serializer_class = serializers.PollSerializer
    queryset = Poll.objects.all()
    permission_classes = [IsResearcher]
    filterset_fields = ['research']

    def permission_denied(self, request, **kwargs):
        if request.user.is_authenticated and self.action in [
            'update',
            'partial_update',
            'list',
            'retrieve',
        ]:
            raise NotFound()
        super().permission_denied(request, **kwargs)

    def get_serializer_class(self):
        if self.action == 'create':
            return serializers.PollSerializer
        elif self.action == 'questions':
            return serializers.QuestionSerializer
        elif self.action == 'answers':
            return serializers.RespondentAnswerSerializer

        return self.serializer_class

    @django_filter_warning(Poll)
    def get_queryset(self):
        user = self.request.user
        queryset = super().get_queryset()
        if self.action == 'list' and not user.is_staff:
            researcher = self.get_researcher()
            queryset = queryset.filter(research__author=researcher)
        return queryset

    def get_permissions(self):
        if self.action == 'create':
            self.permission_classes = [IsResearchAuthor]
        elif self.action == 'list':
            self.permission_classes = [IsResearcher]
        elif self.action == 'questions':
            self.permission_classes = [IsResearchAuthor]
        elif self.action == 'answers':
            self.permission_classes = [IsRespondent]
        elif self.action == 'destroy':
            self.permission_classes = [IsResearchAuthor]
        return super().get_permissions()

    def get_researcher(self):
        return self.request.user.researcher

    def get_respondent(self):
        return self.request.user.respondent

    def get_serializer_context(self):
        context = super().get_serializer_context()
        if hasattr(self.request.user, 'respondent'):
            context['respondent'] = self.get_respondent()
        return context

    @transaction.atomic
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    @transaction.atomic
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @action(['get', 'delete'], detail=True)
    def questions(self, request, *args, **kwargs):
        poll = self.get_object()
        serializer = self.get_serializer
        return self.handle_m2m_actions(poll, request, 'questions', 'id', serializer)

    @action(methods=['POST'], detail=True)
    def answers(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data, many=True)

        if serializer.is_valid(raise_exception=True):
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
