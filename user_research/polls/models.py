from django.core.exceptions import ValidationError
from django.core.validators import (
    MaxLengthValidator,
    MinValueValidator,
    MaxValueValidator,
)
from django.db import models

SHORT_TEXT_LIMIT = 100
LONG_TEXT_LIMIT = 1000

RATING_MIN_VALUE_VALIDATOR = MinValueValidator(limit_value=1)
RATING_MAX_VALUE_VALIDATOR = MaxValueValidator(limit_value=10)


class Poll(models.Model):
    """
    Model representing a polls.

    This model describe the poll title, create and update time
    and set relate between poll and research.
    """

    title = models.CharField('poll title', max_length=SHORT_TEXT_LIMIT)

    created_at = models.DateTimeField('date created', auto_now_add=True)
    updated_at = models.DateTimeField('date updated', auto_now=True)

    research = models.OneToOneField(
        'research.Research',
        on_delete=models.PROTECT,
        related_name='polls',
        verbose_name='research',
    )

    class Meta:
        verbose_name = 'poll'
        verbose_name_plural = 'polls'
        ordering = ['research', '-created_at']

    def __str__(self):
        return f'{self.title[:20]}'


class QuestionType(models.TextChoices):
    SHORT_TEXT = 'stxt', 'короткий текст (до 100 символов)'
    LONG_TEXT = 'ltxt', 'длинный текст (до 1000 символов)'
    CHECK_BOX = 'chb', 'выбор одного варианта'
    MULTI_CHOICE = 'mch', 'выбор нескольких вариантов'
    RATING = 'rate', 'оценка от 1 до 10'


class Question(models.Model):
    """
    Model representing a questions for polls.

    This model describe the question type, question parameters and define
    relate with poll.
    """

    question_type = models.CharField(
        'question type',
        max_length=4,
        choices=QuestionType.choices,
    )
    text = models.TextField('text', validators=[MaxLengthValidator(LONG_TEXT_LIMIT)])
    image = models.ImageField(
        'image',
        upload_to='polls/questions/',
        max_length=255,
        blank=True,
        validators=[],  # TODO: define size and extension validators
    )

    sort_order = models.PositiveSmallIntegerField(
        'sorting order', unique=True, null=True
    )

    is_required = models.BooleanField('mandatory question', default=True)

    poll = models.ForeignKey(
        Poll,
        related_name='questions',
        on_delete=models.CASCADE,
        verbose_name='poll',
    )

    class Meta:
        verbose_name = 'poll\'s question'
        verbose_name_plural = 'poll questions'
        ordering = ['poll', 'sort_order']
        unique_together = ['poll', 'sort_order']

    def __str__(self):
        return f'{self.poll}, {self.text[:20]}'


class AnswerOptions(models.Model):
    """
    Model representing answers variants for poll question.

    Answer may be as text, or image or integer rating. Answer related
    with question and answer type must be matches question type.
    """

    text = models.TextField(
        blank=True,
        verbose_name='text',
        validators=[MaxLengthValidator(LONG_TEXT_LIMIT)],
    )
    image = models.ImageField(
        upload_to='polls/answers/',
        max_length=255,
        blank=True,
        verbose_name='image',
        validators=[],  # TODO: define size and extension validators
    )

    rating_min = models.PositiveSmallIntegerField(
        'minimum rating',
        null=True,
        blank=True,
        validators=[
            RATING_MIN_VALUE_VALIDATOR,
            RATING_MAX_VALUE_VALIDATOR,
        ],
    )
    rating_max = models.PositiveSmallIntegerField(
        'maximum rating',
        null=True,
        blank=True,
        validators=[
            RATING_MIN_VALUE_VALIDATOR,
            RATING_MAX_VALUE_VALIDATOR,
        ],
    )
    sort_order = models.PositiveSmallIntegerField(
        'sorting order', unique=True, null=True
    )

    question = models.ForeignKey(
        Question,
        on_delete=models.CASCADE,
        related_name='answer_options',
        verbose_name='question',
        blank=True,
        null=True,
    )

    class Meta:
        verbose_name = 'answer options'
        verbose_name_plural = 'answers options'
        ordering = ['question', 'sort_order']
        unique_together = ['question', 'sort_order']

    def __str__(self):
        return f'{self.question}, {self.text[:20]}'


class RespondentAnswer(models.Model):
    """
    Model for respondent answer.

    Answer may be as text, or image or integer rating.
    Respondent answer related by respondent profile and poll question.
    Answer  must be matches question type.
    """

    text = models.TextField(
        'text',
        blank=True,
        validators=[MaxLengthValidator(LONG_TEXT_LIMIT)],
    )
    rating = models.PositiveSmallIntegerField('rating', null=True, blank=True)

    respondent = models.ForeignKey(
        'respondent.RespondentProfile',
        on_delete=models.CASCADE,  # TODO: Delete answer if User deleted?
        related_name='respondent_answers',
        verbose_name='respondent',
    )

    question = models.ForeignKey(
        Question,
        on_delete=models.CASCADE,
        related_name='respondents_answers',
        verbose_name='question',
    )

    answer_options = models.ManyToManyField(
        AnswerOptions,
        related_name='respondent_answers',
        verbose_name='answer options',
        blank=True,
    )

    class Meta:
        verbose_name = 'respondent\'s answer'
        verbose_name_plural = 'respondent answers'
        ordering = ['respondent', 'question']

    def __str__(self):
        return f'{self.respondent}, {self.question}'
