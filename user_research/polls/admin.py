from django.contrib import admin

from . import models


@admin.register(models.Poll)
class PollAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'title',
        'research',
    )
    list_display_links = ('title',)
    search_fields = (
        'research',
        'title',
    )


@admin.register(models.Question)
class QuestionAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'poll',
        'sort_order',
        'text',
    )
    list_display_links = (
        'id',
        'text',
    )
    search_fields = (
        'poll',
        'text',
    )


@admin.register(models.AnswerOptions)
class AnswerOptionAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'question',
        'text',
    )
    list_display_links = (
        'id',
        'text',
    )
    search_fields = (
        'question',
        'text',
    )


@admin.register(models.RespondentAnswer)
class RespondentAnswerAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'respondent',
        'question',
    )
    list_display_links = (
        'id',
        'question',
    )
    search_fields = ('question',)
