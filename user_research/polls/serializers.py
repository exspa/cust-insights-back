from rest_framework import serializers
from drf_writable_nested import WritableNestedModelSerializer

from . import models
from research.models import Research


TEXT_QUESTION_TYPES = [models.QuestionType.SHORT_TEXT, models.QuestionType.LONG_TEXT]
CHOICE_QUESTION_TYPES = [
    models.QuestionType.CHECK_BOX,
    models.QuestionType.MULTI_CHOICE,
]


class AnswerOptionsSerializer(serializers.ModelSerializer):
    image = serializers.ImageField(required=False)
    sort_order = serializers.IntegerField(required=False)

    class Meta:
        model = models.AnswerOptions
        exclude = ['question']


class QuestionSerializer(WritableNestedModelSerializer):
    image = serializers.ImageField(required=False)
    sort_order = serializers.IntegerField(required=False)

    answer_options = AnswerOptionsSerializer(many=True, required=False)

    class Meta:
        model = models.Question
        exclude = ['poll']

    def validate(self, attr):
        question_type = attr.get('question_type')
        answers_count = 0

        if answer_options := attr.get('answer_options'):
            answers_count = len(answer_options)

        if question_type in TEXT_QUESTION_TYPES and answers_count:
            raise serializers.ValidationError(
                {f'{question_type}': 'Text questions should not have answer options.'}
            )

        if question_type in CHOICE_QUESTION_TYPES and (
            answers_count < 2 or answers_count > 10
        ):
            raise serializers.ValidationError(
                {
                    f'{question_type}': f'Expected 2-10 options for this question type '
                    f'but received {answers_count}.'
                }
            )

        if question_type == models.QuestionType.RATING:
            rating_min = rating_max = None

            for option in answer_options:
                if 'rating_min' in option:
                    rating_min = option['rating_min']
                if 'rating_max' in option:
                    rating_max = option['rating_max']

            if not rating_min or not rating_max or rating_min >= rating_max:
                raise serializers.ValidationError(
                    {
                        f'{question_type}': 'Invalid rating range. Ensure that the '
                        'minimum is less than the maximum.'
                    }
                )
            if answers_count > 2:
                raise serializers.ValidationError(
                    {
                        f'{question_type}': 'Only 2 options (min and max) are expected '
                        'for a rating question.'
                    }
                )

        return attr


class PollSerializer(WritableNestedModelSerializer):
    research = serializers.PrimaryKeyRelatedField(queryset=Research.objects.all())
    questions = QuestionSerializer(many=True)

    class Meta:
        model = models.Poll
        exclude = ['created_at', 'updated_at']


class CurrentRespondentDefault(serializers.CurrentUserDefault):
    def __call__(self, serializer_field):
        return serializer_field.context['respondent']


class RespondentAnswerSerializer(WritableNestedModelSerializer):
    text = serializers.CharField(required=False)
    rating = serializers.IntegerField(required=False)

    respondent = serializers.HiddenField(default=CurrentRespondentDefault())
    question = serializers.PrimaryKeyRelatedField(
        queryset=models.Question.objects.all()
    )
    answer_options = AnswerOptionsSerializer(many=True, required=False)

    class Meta:
        model = models.RespondentAnswer
        fields = '__all__'

    def validate(self, attr):  # TODO(DRY): Refactor, add mixin for validate
        text = attr.get('text')
        rating = attr.get('rating')
        question = attr.get('question')
        answer_options = attr.get('answer_options')
        question_type = question.question_type

        def check_question_type(q_id, expected_field, other_fields):
            if attr.get(expected_field) and any(
                attr.get(field) for field in other_fields
            ):
                raise serializers.ValidationError(
                    {f'{q_id}': f'Expected only "{expected_field}".'}
                )

        if question.is_required:
            if question_type in TEXT_QUESTION_TYPES and not text:
                raise serializers.ValidationError(
                    {f'{question.id}': 'Expected "text".'}
                )

            if question_type in CHOICE_QUESTION_TYPES and not answer_options:
                raise serializers.ValidationError(
                    {f'{question.id}': 'Expected "answer options".'}
                )

            if question_type == models.QuestionType.RATING and not rating:
                raise serializers.ValidationError(
                    {f'{question.id}': 'Expected "rating".'}
                )

        if text or rating or answer_options:
            if question_type in TEXT_QUESTION_TYPES:
                check_question_type(question.id, 'text', ['rating', 'answer_options'])

            elif question_type in CHOICE_QUESTION_TYPES:
                check_question_type(question.id, 'answer_options', ['text', 'rating'])

            elif question_type == models.QuestionType.RATING:
                check_question_type(question.id, 'rating', ['text', 'answer_options'])

        # TODO: Extend the scenarios of validation, add comparison
        #  of answer options

        return attr
