stages:
  - pre-build
  - test
  - build
  - deploy
  - trigger

variables:
  DOCKER_REGISTRY: ${DOCKER_REGISTRY}
  DOMAIN_NAME: custinsights.ru
  DOCKER_DEV_HOST: ssh://$SF_DEV_USER@$SF_DEV_HOST

create-builder:
  stage: pre-build
  tags: [shell]
  rules:
    - if: $CI_PIPELINE_SOURCE == "push" || $CI_COMMIT_BRANCH =~ /^build\/.*/
      changes:
        - DockerfileBuilder
        - user_research/requirements.txt
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker pull $CI_REGISTRY_IMAGE:builder || exit_code=$?
  script:
    - >
      docker build --cache-from $CI_REGISTRY_IMAGE:builder
      --file DockerfileBuilder
      --tag $CI_REGISTRY_IMAGE:builder
      --build-arg BUILD_VERSION=${BUILD_VERSION} .
    - docker push $CI_REGISTRY_IMAGE:builder

flake8:
  stage: test
  rules:
    - if: $CI_COMMIT_BRANCH =~ /^build\/.*/
      when: never
    - if: $CI_PIPELINE_SOURCE == "push"
      changes:
        - "**/*.py"
  before_script:
    - python -m pip install flake8
  script:
    - flake8 user_research/

bandit:
  stage: test
  rules:
    - if: $CI_COMMIT_BRANCH =~ /^build\/.*/
      when: never
    - if: $CI_PIPELINE_SOURCE == "push"
      changes:
        - "**/*.py"
  before_script:
    - python -m pip install bandit
  script:
    - bandit --exclude tests -r user_research/

pyrun:
  stage: test
  image: $CI_REGISTRY_IMAGE:builder
  rules:
    - if: $CI_COMMIT_BRANCH =~ /^build\/.*/
      when: never
  before_script:
    - export SECRET_KEY=$(tr -dc 'a-z0-9' < /dev/urandom | head -c50)
  script:
    - >
      cd user_research
      && gunicorn --bind 0.0.0.0:8000
      --workers=1 user_research.wsgi:application &
    - sleep 7
    - curl -f localhost:8000/ht/ || exit 1

build-image:
  stage: build
  tags: [shell]
  variables:
    BUILD_VERSION: ${CI_COMMIT_SHORT_SHA}
  rules:
    - if: $CI_COMMIT_BRANCH == "dev"
    - if: $CI_COMMIT_BRANCH =~ /^build\/.*/
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker stop testback || exit_code=$?
  script:
    - docker pull $CI_REGISTRY_IMAGE:latest || exit_code=$?
    - >
      docker build --cache-from $CI_REGISTRY_IMAGE:latest
      --tag $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA
      --tag $CI_REGISTRY_IMAGE:latest
      --build-arg BUILDER=$CI_REGISTRY_IMAGE:builder
      --build-arg BUILD_VERSION=${BUILD_VERSION} .
    - docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA
    - docker push $CI_REGISTRY_IMAGE:latest
    - >
      docker run -d --rm -p 8222:8000
      --name testback --env TEST_RUN=True
      $CI_REGISTRY_IMAGE:latest
    - sleep 7
    - >
      curl localhost:8222/ht/
      | grep $CI_COMMIT_SHORT_SHA || exit 1
    - docker logs testback && docker stop testback

deploy:
  stage: deploy
  tags: [shell]
  variables:
    DOCKER_NETWORK_ID: net
    DOMAIN_NAME: custinsights.ru
    ALLOWED_HOSTS: "$DOMAIN_NAME,api.$DOMAIN_NAME"
  rules:
    - if: $CI_COMMIT_BRANCH =~ /^build\/.*/
      when: never
    - if: $CI_COMMIT_BRANCH == "dev"
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - eval $(ssh-agent -s)
    - chmod 400 "$SSH_PRIVATE_KEY"
    - ssh-add "$SSH_PRIVATE_KEY"
  script:
    - docker -H $DOCKER_DEV_HOST pull $CI_REGISTRY_IMAGE:latest
    - >
      docker -H $DOCKER_DEV_HOST stack deploy
      --with-registry-auth -c docker-swarm.yml app
    - .gitlab/curl-health-check.sh

collectstatic:
  needs: [deploy]
  stage: deploy
  image: $CI_REGISTRY_IMAGE:builder
  variables:
    SECRET_KEY: ${PROD_SECRET_KEY}
  rules:
    - if: $CI_COMMIT_BRANCH == "dev"
  script:
    - >
      cd user_research
      && python manage.py collectstatic --noinput --clear

build-amd:
  stage: trigger
  rules:
    - if: $CI_COMMIT_BRANCH == "dev"
    - if: $CI_COMMIT_BRANCH =~ /^build\/.*/
  allow_failure: true
  trigger:
    include:
      - local: .gitlab/build-amd.yml
    forward:
      pipeline_variables: true
      yaml_variables: true
