#!/usr/bin/env bash

HEALTH_CHECK_URL="https://api.${DOMAIN_NAME}/ht/"

# количество попыток
ATTEMPTS=5

# задержка между попытками (в секундах)
DELAY=7

# Счётчик попыток
attempt_counter=0

sleep 30
# Цикл попыток
while [ ${attempt_counter} -lt ${ATTEMPTS} ]
do
    # Сам тест: проверяем сайт с помощью cURL
    curl -m 5 --fail ${HEALTH_CHECK_URL} | grep ${CI_COMMIT_SHORT_SHA}

    # Если curl завершается с кодом ошибки, то инкрементируем счётчик, иначе выходим из цикла
    if [ $? -ne 0 ]; then
        attempt_counter=$(($attempt_counter+1))
        sleep ${DELAY}
    else
        exit 0
    fi
done

echo "Failed to reach $HEALTH_CHECK_URL after $ATTEMPTS attempts"
exit 1
